<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dados-pessoais','Admin\AdminController@personalInfo')->name('dados-pessoais');
    Route::post( 'api/login', 'Auth\LoginController@login' );
    Route::post( '/authenticate', 'Auth\LoginController@authenticate' );
    Route::get( 'logout', 'Auth\LoginController@logout' );
    Route::get( '/login', 'Admin\FranchiseeController@login' )->name( 'login' ); //mover

Route::middleware( ['auth', 'adminLevel'] )->group(function() {
    /* Admin Manager Routes */
    Route::get( '/admin', 'Admin\AdminController@dashboard' );
    Route::get( '/admin/financeiro', 'Admin\AdminController@financial' );

    Route::get( '/admin/franqueados', 'Admin\AdminController@franchisees' );
    Route::get( '/admin/franqueados/add', 'Admin\AdminController@createFranchisee' );
    Route::get( '/admin/franqueados/{hash_id}', 'Admin\AdminController@editFranchisee' );

    Route::get( '/admin/empresas', 'Admin\AdminController@companies' );

    Route::get( '/admin/planos', 'Admin\AdminController@plans' );
    Route::get( '/admin/planos/add', 'Admin\AdminController@createPlan' );
    Route::get( '/admin/planos/{hash_id}', 'Admin\AdminController@editPlan' );

    Route::get( '/admin/categorias', 'Admin\AdminController@categories' );
    Route::get( '/admin/categorias/add', 'Admin\AdminController@createCategory' );
    Route::get( '/admin/categorias/{hash_id}', 'Admin\AdminController@editCategory' );
});

/* Fanchise Manager Routes */
Route::middleware( ['auth', 'franchiseeLevel'] )->group(function() {
    Route::get('/franqueado','Admin\FranchiseeController@dashboard');
    Route::get('/franqueado/financeiro','Admin\FranchiseeController@financial');

    Route::get('/franqueado/empresas','Admin\FranchiseeController@companies');
    Route::get('/franqueado/empresas/add','Admin\FranchiseeController@createCompany');
    Route::get('/franqueado/empresas/{hash_id}','Admin\FranchiseeController@editCompany');

    Route::get('/franqueado/eventos','Admin\FranchiseeController@events');
    Route::get('/franqueado/eventos/add','Admin\FranchiseeController@createEvent');
    Route::get('/franqueado/eventos/{hash_id}','Admin\FranchiseeController@editEvent');

    Route::get('/franqueado/noticias','Admin\FranchiseeController@news');
    Route::get('/franqueado/noticias/add','Admin\FranchiseeController@createNew');
    Route::get('/franqueado/noticias/{hash_id}','Admin\FranchiseeController@editNew');

    Route::get('/franqueado/pontos-turisticos','Admin\FranchiseeController@tourSpots');
    Route::get('/franqueado/pontos-turisticos/add','Admin\FranchiseeController@createTourSpot');
    Route::get('/franqueado/pontos-turisticos/{hash_id}','Admin\FranchiseeController@editTourSpot');

    Route::get('/franqueado/galerias','Admin\FranchiseeController@galleries');
    Route::get('/franqueado/galerias/add','Admin\FranchiseeController@createGallery');
    Route::get('/franqueado/galerias/{hash_id}','Admin\FranchiseeController@editGallery');

    Route::get('/franqueado/categorias','Admin\FranchiseeController@categories');
    Route::get('/franqueado/categorias/add','Admin\FranchiseeController@createCategory');
    Route::get('/franqueado/categorias/{hash_id}','Admin\FranchiseeController@editCategory');
});

/* Company Manager Routes */
Route::middleware( ['auth', 'companyLevel'] )->group(function() {
    Route::get('/empresa','Admin\CompanyController@dashboard');
    Route::get('/empresa/editar','Admin\CompanyController@edit');
});







//////////////////////////////////////SITE////////////////////////////////////////


Route::get('/{city}','Site\CityController@home')->name('city');
Route::get('/{city}/categorias/{category}','Site\CityController@empresas')->name('category');
Route::get('/{city}/empresas/{company}','Site\CityController@empresa')->name('company');




