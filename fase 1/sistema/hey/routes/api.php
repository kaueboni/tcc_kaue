<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/upload/image','UploadController@store');
Route::post('/upload/image/delete','UploadController@destroy');

//admin
Route::post('/categories_defaults','Api\CategoryDefaultController@store');
Route::delete('/categories_defaults/{id}','Api\CategoryDefaultController@destroy');
Route::patch('/categories_defaults/{id}','Api\CategoryDefaultController@update');

Route::post('/plans','Api\PlanController@store');
Route::delete('/plans/{id}','Api\PlanController@destroy');
Route::patch('/plans/{id}','Api\PlanController@update');

//franchisee
Route::post('/franchisees/candidates','Api\FranchiseeCandidateController@store');
Route::delete('/franchisees/candidates/{id}','Api\FranchiseeCandidateController@destroy');
Route::get('/franchisees/candidates/accept/{id}','Api\FranchiseeController@store');

Route::post('/events','Api\EventController@store');
Route::patch('/events/{id}','Api\EventController@update');
Route::delete('/events/{id}','Api\EventController@destroy');

Route::post('/gallery_posts','Api\GalleryPostController@store');
Route::patch('/gallery_posts/{id}','Api\GalleryPostController@update');
Route::delete('/gallery_posts/{id}','Api\GalleryPostController@destroy');

Route::post('/news','Api\NewsController@store');
Route::patch('/news/{id}','Api\NewsController@update');
Route::delete('/news/{id}','Api\NewsController@destroy');

Route::post('/tourspots','Api\TourSpotController@store');
Route::patch('/tourspots/{id}','Api\TourSpotController@update');
Route::delete('/tourspots/{id}','Api\TourSpotController@destroy');

Route::post('/categories','Api\CategoryController@store');
Route::patch('/categories/{id}','Api\CategoryController@update');
Route::delete('/categories/{id}','Api\CategoryController@destroy');

Route::patch('/users/update','Api\UserController@update');

//companies
Route::post('/companies/candidates','Api\CompanyCandidateController@store');
Route::delete('/companies/candidates/{id}','Api\CompanyCandidateController@destroy');
Route::get('/companies/candidates/accept/{id}','Api\CompanyController@store');

Route::patch('/companies/{hash_id}','Api\CompanyController@update');
Route::delete('/companies/{hash_id}','Api\CompanyController@destroy');

