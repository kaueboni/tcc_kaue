<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
        'title',
        'subtitle',
        'content',
        'gallery_id',
        'franchisee_id'
    ];

    public function franchisee() {
        return $this->belongsTo( 'App\Models\Franchisee' );
    }


}
