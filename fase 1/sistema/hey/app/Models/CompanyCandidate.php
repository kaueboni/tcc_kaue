<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyCandidate extends Model
{
    protected $table = 'companies_candidates';

    protected $fillable = [
        'company_name',
        'primary_phone',
        'street_name',
        'street_number',
        'complement',
        'district',
        'zipcode',
        'city_name',
        'city_state',
        'owner_name',
        'owner_phone',
        'owner_cellphone',
        'owner_email',
        'franchisee_id'
    ];
}
