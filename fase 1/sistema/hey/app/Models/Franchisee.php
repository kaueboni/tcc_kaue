<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Franchisee extends Model
{

    protected $table = 'franchisees';

    protected $fillable = [
        'city_name',
        'city_state',
        'user_id'
    ];

    public function companies(){
        return $this->hasMany('App\Models\Company');
    }

    public function events(){
        return $this->hasMany('App\Models\Event');
    }

    public function news(){
        return $this->hasMany('App\Models\News');
    }

    public function tourspots(){
        return $this->hasMany('App\Models\TourSpot');
    }

    public function galleries(){
        return $this->hasMany('App\Models\GalleryPost');
    }

    public function categories(){
        return $this->hasMany('App\Models\Category');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
