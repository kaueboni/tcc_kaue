<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourSpot extends Model
{
    protected $table = 'tour_spots';

    protected $fillable = [
        'title',
        'info',
        'longitude',
        'latitude',
        'franchisee_id',
        'type'
    ];

    public function franchisee() {
        return $this->belongsTo( 'App\Models\Franchisee' );
    }


}
