<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'parent_id',
        'level',
        'slug',
        'name',
        'franchisee_id',
    ];

    public function franchisee(){
        return $this->belongsTo('App\Models\Franchisee');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Category','parent_id');
    }

    public function companies(){
        return $this->belongsToMany('App\Models\Company');
    }

    public function childrens(){
        return $this->hasMany('App\Models\Category','parent_id');
    }
}
