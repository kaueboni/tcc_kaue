<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchiseeCandidate extends Model {
    protected $table = 'franchisee_candidates';

    protected $fillable = [
        'city_name',
        'city_state',
        'owner_name',
        'owner_phone',
        'owner_cellphone',
        'owner_email',
    ];

}
