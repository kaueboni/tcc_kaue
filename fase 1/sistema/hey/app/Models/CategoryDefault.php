<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryDefault extends Model
{
    protected $table = 'categories_defaults';

    protected $fillable = [
        'name',
        'slug',
        'parent_id',
        'level'
    ];

    public function parent(){
        return $this->belongsTo('App\Models\CategoryDefault', 'parent_id');
    }
}
