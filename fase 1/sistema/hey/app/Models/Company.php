<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = [
        'name',
        'summary',
        'description',
        'primary_phone',
        'secondary_phone',
        'cellphone',
        'email',
        'street_name',
        'street_number',
        'complement',
        'district',
        'zipcode',
        'city',
        'state',
        'user_id',
        'franchisee_id',
        'menu',
        'gallery',
        'latitude',
        'longitude'
    ];

    public function franchisee() {
        return $this->belongsTo('App\Models\Franchisee');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Category');
    }
}
