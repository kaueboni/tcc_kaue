<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryPost extends Model
{
    protected $table = 'gallery_posts';

    protected $fillable = [
        'name',
        'date',
        'gallery_id',
        'franchisee_id',
    ];

    public function franchisee() {
        return $this->belongsTo( 'App\Models\Franchisee' );
    }
}
