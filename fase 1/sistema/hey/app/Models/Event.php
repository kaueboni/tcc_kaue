<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    protected $fillable = [
        'name',
        'date',
        'location_name',
        'longitude',
        'latitude',
        'info',
        'gallery_id',
        'franchisee_id',
    ];

    public function franchisee(){
        return $this->belongsTo('App\Models\Franchisee');
    }
}
