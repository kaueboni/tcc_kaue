<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'cpf',
        'rg',
        'phone',
        'cellphone',
        'birthdate',
        'plan',
        'level',
        'personal_info',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function franchisee(){
        return $this->hasOne('App\Models\Franchisee', 'user_id');
    }

    public function company(){
        return $this->hasOne('App\Models\Company', 'user_id');
    }
}
