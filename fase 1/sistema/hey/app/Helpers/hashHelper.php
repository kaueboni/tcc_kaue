<?php

use App\Services\HashidsService;

if(!function_exists('hashid_encode')){

    function hashid_encode($value){
        $h = new HashidsService();
        $h = $h->getHashids();
        return $h->encode($value);
    }

}


if(!function_exists('hashid_decode')){

    function hashid_decode($value){
        $h = new HashidsService();
        $h = $h->getHashids();
        return $h->decode($value);
    }

}