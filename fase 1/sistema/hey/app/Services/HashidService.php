<?php


namespace App\Services;

use Hashids\Hashids;

class HashidsService{

    public function getHashids(){

        return new Hashids('HeyCidades',10,'ABCDEFGHJKLMNPQRSTUVWXYZ23456789');

    }

}
