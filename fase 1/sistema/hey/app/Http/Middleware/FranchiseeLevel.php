<?php

namespace App\Http\Middleware;

use Closure;

class FranchiseeLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! auth()->check() ) {
            return redirect()->route( 'login' );
        }

        if ( auth()->user()->personal_info != 1 ) {
            return redirect('dados-pessoais')->with('flash', 'Atualize suas informações antes de prosseguir!');
        }

        if ( auth()->user()->level != 2 ) {
            return redirect('logout');
        }

        return $next( $request );
    }
}
