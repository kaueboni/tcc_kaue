<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UploadController extends Controller
{
    public function __construct() {
        $this->photos_path = public_path( '/upload' );
    }

    public function store( Request $request ) {

        try{

            $photos = $request->file( 'file' );

            $dir_name = Carbon::now()->month;

            if ( ! is_array( $photos ) ) {
                $photos = [ $photos ];
            }

            if ( ! is_dir( $this->photos_path . '/' . $dir_name ) ) {
                mkdir( $this->photos_path . '/' . $dir_name, 0777 );
            }

            for ( $i = 0; $i < count( $photos ); $i ++ ) {
                $photo    = $photos[ $i ];
                $name     = date( 'His_dmy' ) . "_" . str_random( 3 );
                $filename = $name . '.' . $photo->getClientOriginalExtension();

                Image::make( $photo )->resize( 600, 450, function ( $constraints ) {
                    $constraints->aspectRatio();
                } )->fit( 600 )->save( $this->photos_path . '/' . $dir_name . '/' . $filename );
                //$photo->move($this->photos_path. '/' . $dir_name, $save_name);

            }

            return response([
                'status' => 'ok',
                'path' => '/upload/'.$dir_name.'/'.$filename
            ], 200 );

        } catch (\Exception $e) {



            return response([
                'status' => 'error',
                'message' => 'Não foi possível fazer o upload.'
            ], 500 );

        }

    }


    public function destroy(Request $request){

        $gallery_id = $request->input('gallery_id');
        $file = $request->input('path');
        $images = $request->input('gallery');
        $gallery = Gallery::find($gallery_id);
        $i = 0;

        try{
            unlink(public_path($file));
            foreach($images as $img) {
                if($img == $file)
                    $key = $i;
                $i++;
            }
            unset($images[$i-1]);
            $gallery->images = serialize($images);
            $gallery->save();

            return response([
                'code' => '1',
                'message' => 'A imagem foi apagada.'
            ], 200 );
        } catch(\Exception $e){

            dd($e);
            return response([
                'code' => '10',
                'message' => 'Não foi possível remover a imagem.'
            ], 500 );
        }

    }

    public function updateGallery(Request $request){
        $gallery_id = $request->input('gallery_id');
        $images = $request->input('gallery');
        try{
            $gallery = Gallery::find(hashid_decode($gallery_id))->first();
            $gallery->images = serialize($images);
            $gallery->save();
            return response([
                'code' => '1',
                'message' => 'A galeria foi atualizada.'
            ], 200 );
        }catch(\Exception $e){
            return response([
                'code' => '10',
                'message' => 'Não foi possível atualizar a galeria.'
            ], 500);
        }

    }


}
