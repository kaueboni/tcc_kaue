<?php

namespace App\Http\Controllers\Api;

use App\Models\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller
{

    public function store( Request $request ) {

        $data   = $request->input();

        $rules = array(
            'name'          => 'required',
            'description'   => 'required',
            'price'         => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        $data['price'] =  preg_replace( '/[^0-9]/', '', $data['price'] );

        DB::beginTransaction();
        try {

            $plan = Plan::create( $data );
            DB::commit();
            return response( $plan->toJson(), 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }


    }

    public function update( Request $request, $id ) {

        $data   = $request->input();

        $rules = array(
            'name'          => 'required',
            'description'   => 'required',
            'price'         => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        $data['price'] =  preg_replace( '/[^0-9]/', '', $data['price'] );

        DB::beginTransaction();
        try {

            $plan = Plan::find( hashid_decode($id) )->first();
            $plan->update($data);

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'O plano foi salvo.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();
            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }

    }

    public function destroy( $id ) {

        DB::beginTransaction();

        try{

            $plan = Plan::find(hashid_decode($id))->first();
            $plan->delete();

            DB::commit();
            return response( [
                'code' => 1,
                'message' => 'Registro removido.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();
            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
