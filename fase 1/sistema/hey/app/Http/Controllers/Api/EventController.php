<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Gallery;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller {

    public function store( Request $request ) {

        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );

        $rules = array(
            'name'          => 'required',
            'date'          => 'required|data',
            'location_name' => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['date'] = Carbon::createFromFormat( 'd/m/Y - H:i', $data['date'] . ' - ' . $data['hour'] );

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];

            $event           = Event::create( $data );

            $gallery         = new Gallery();
            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $event->gallery_id = $gallery->id;
            $event->save();



            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'O evento foi salvo.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }

    }

    public function update( Request $request, $id ) {

        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );

        $rules = array(
            'name'          => 'required',
            'date'          => 'required|data',
            'location_name' => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['date'] = Carbon::createFromFormat( 'd/m/Y - H:i', $data['date'] . ' - ' . $data['hour'] );

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];

            $data['gallery_id'] = hashid_decode($data['gallery_id'])[0];

            $event = Event::find( hashid_decode($id) )->first();

            $event->update($data);

            if(isset($data['gallery_id'])){
                $gallery = Gallery::find($data['gallery_id']);
            }else{
                $gallery = new Gallery();
            }

            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $event->gallery_id = $gallery->id;
            $event->save();

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'O evento foi salvo.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }

    }

    public function destroy( $id ) {

        DB::beginTransaction();

        try{

            $id = hashid_decode($id);
            $event = Event::find($id)->first(); //first pq o hashid é array
            $gallery = Gallery::find($event->gallery_id);

            if($gallery instanceof Gallery){
                $gallery->delete();
            }
            $event->delete();

            DB::commit();

            return response( [
                'code' => 1,
                'message' => 'Registro removido.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();

            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
