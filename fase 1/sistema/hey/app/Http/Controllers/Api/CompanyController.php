<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\FranchiseeAccepted;
use App\Models\Category;
use App\Models\Company;
use App\Models\CompanyCandidate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( $id ) {
        //obtem data do candidato
        $candidate = CompanyCandidate::find( hashid_decode( $id ) )->first();

        //verifica se nao ha algum franqueado com o mesmo email
        if ( User::where( 'email', $candidate->owner_email )->get()->count() > 0 ) {
            return response( [
                'code'    => 10,
                'message' => 'Já existe um usuário com o mesmo e-mail.'
            ], 500 );
        }

        DB::beginTransaction();
        try {

            //senha é o hash do id do candidato
            $password = hashid_encode( $candidate->id );

            //cria um usuario
            $user = User::create( [
                'name'     => $candidate->owner_name,
                'email'    => $candidate->owner_email,
                'password' => bcrypt( $password ),
            ] );

            $user->level     = 1;
            $user->phone     = $candidate->owner_phone;
            $user->cellphone = $candidate->owner_cellphone;
            $user->save();


            //cria a empresa
            $company = Company::create( [
                'name'          => $candidate->company_name,
                'primary_phone' => $candidate->primary_phone,
                'slug'          => str_slug( $candidate->company_name ),
                'street_name'   => $candidate->street_name,
                'street_number' => $candidate->street_number,
                'zipcode'       => $candidate->zipcode,
                'district'      => $candidate->district,
                'complement'    => $candidate->complement,
                'city'          => $candidate->city_name,
                'state'         => $candidate->city_state,
                'user_id'       => $user->id,
                'franchisee_id' => $candidate->franchisee_id
            ] );

            Mail::to( $user->email )->send( new FranchiseeAccepted( $user, $password ) );

            $candidate->delete();

            DB::commit();

            return response( $company->toJson(), 200 );


        } catch ( \Exception $e ) {

            dd( $e );

            DB::rollBack();

            return response( [
                'code'    => 12,
                'message' => 'Não foi possível criar a franquia.'
            ], 500 );
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {

        $data       = $request->input( 'formData' );
        $images     = $request->input( 'gallery' );
        $categories = $request->input( 'categories' );

        $rules = array(
            'name'            => 'required',
            'primary_phone'   => 'required',
            'street_name'     => 'required',
            'street_number'   => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        $data['gallery'] = serialize($images);


        DB::beginTransaction();
        try {

            $company = Company::find(hashid_decode($data['company_id']))->first();
            $company->update($data);
            $company->categories()->detach();

            if(is_array($categories)){
                foreach($categories as $cat){
                    $category = Category::find($cat);
                    $company->categories()->attach($category);
                }
            }

            $response = [
                'company' => $company->toJson(),
                'categories' => $company->categories->toJson()
            ];

            DB::commit();
            return response( $response, 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => 'Não foi possível atualizar a empresa.'
            ], 500 );
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        DB::beginTransaction();
        try {

            $id        = hashid_decode( $id );
            $candidate = Company::find( $id )->first(); //first pq o hashid é array
            $candidate->delete();

            DB::commit();

            return response( [
                'code'    => 1,
                'message' => 'Registro removido.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
