<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data   = $request->input();

        $rules = array(
            'name'          => 'required',
            'parent_id'     => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        $categories = Category::where('franchisee_id',$data['franchisee_id']);

        if($categories->where('name',$data['name'])->get()->count()>0){
            return response( [
                'code'    => 11,
                'message' => 'Já existe uma categoria com esse nome nessa franquia.'
            ], 500 );
        }

        $data['level'] = 2;
        $data['slug'] = str_slug($data['name']);

        DB::beginTransaction();
        try {

            $category = Category::create( $data );

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A categoria foi adicionada.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();
            return response( [
                'code'    => 12,
                'message' => 'Não foi possível salvar.'
            ], 500 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data   = $request->input();
        $id = hashid_decode($id)[0];

        $rules = array(
            'name'          => 'required',
            'parent_id'     => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        $categories = Category::where('franchisee_id',$data['franchisee_id']);

        $cat = $categories->where('name',$data['name'])->first();

        if($cat instanceof Category && $cat->id != $id){
            return response( [
                'code'    => 11,
                'message' => 'Já existe uma categoria com esse nome nessa franquia.'
            ], 500 );
        }

        $data['level'] = 2;
        $data['slug'] = str_slug($data['name']);

        DB::beginTransaction();
        try {

            $category = Category::find($id);
            $category->update($data);

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A categoria foi atualizada.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();
            return response( [
                'code'    => 12,
                'message' => 'Não foi possível atualizar.'
            ], 500 );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $id = hashid_decode($id);
            $cat = Category::find($id)->first(); //first pq o hashid é array
            $cat->delete();

            DB::commit();
            return response( [
                'code' => 1,
                'message' => 'Categoria removida.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();
            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
