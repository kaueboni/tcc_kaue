<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\FranchiseeAccepted;
use App\Models\Category;
use App\Models\CategoryDefault;
use App\Models\Franchisee;
use App\Models\FranchiseeCandidate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class FranchiseeController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( $id ) {
        //obtem data do candidato
        $candidate = FranchiseeCandidate::find( hashid_decode( $id ) )->first();

        //verifica se nao ha algum franqueado com o mesmo email
        if ( User::where( 'email', $candidate->owner_email )->get()->count() > 0 ) {
            return response( [
                'code'    => 10,
                'message' => 'Já existe um franqueado com o mesmo e-mail.'
            ], 500 );
        }

        if ( Franchisee::where( 'city_name', $candidate->city_name )->get()->count() > 0 ) {
            return response( [
                'code'    => 11,
                'message' => 'Já existe um franqueado nesta cidade.'
            ], 500 );
        }

        DB::beginTransaction();
        try {

            //senha é o hash do id do candidato
            $password = hashid_encode( $candidate->id );

            //cria um usuario
            $user = User::create( [
                'name'     => $candidate->owner_name,
                'email'    => $candidate->owner_email,
                'password' => bcrypt( $password ),
            ] );

            $user->level     = 2;
            $user->phone     = $candidate->owner_phone;
            $user->cellphone = $candidate->owner_cellphone;
            $user->save();

            //cria a franquia
            $franchisee = Franchisee::create( [
                'city_state' => $candidate->city_state,
                'city_name'  => $candidate->city_name,
                'slug'       => str_slug($candidate->city_name),
                'user_id'    => $user->id
            ] );

            $franchisee->slug = str_slug($candidate->city_name);
            $franchisee->save();

            //povoando as categorias
            $categories = CategoryDefault::all();
            if ( $categories->count() > 0 ) {
                foreach ( $categories as $cat ) {

                    $category = [
                        'name'  => $cat->name,
                        'level' => $cat->level,
                        'slug'  => $cat->slug,
                        'franchisee_id' => $franchisee->id
                    ];

                    Category::create($category);

                }
            }

            Mail::to( $user->email )->send( new FranchiseeAccepted( $user, $password ) );

            $candidate->delete();

            DB::commit();

            return response( $franchisee->toJson(), 200 );


        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 12,
                'message' => 'Não foi possível criar a franquia.'
            ], 500 );
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id ) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {
        //
    }
}
