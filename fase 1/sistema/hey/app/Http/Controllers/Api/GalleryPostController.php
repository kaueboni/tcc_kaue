<?php

namespace App\Http\Controllers\Api;

use App\Models\Gallery;
use App\Models\GalleryPost;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GalleryPostController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );

        $rules = array(
            'name'          => 'required',
            'date'          => 'required|data'
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['date'] = Carbon::createFromFormat( 'd/m/Y', $data['date'] );

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];

            $gallery_post           = GalleryPost::create( $data );

            $gallery         = new Gallery();
            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $gallery_post->gallery_id = $gallery->id;
            $gallery_post->save();

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A galeria foi salva.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );
        $images = serialize($images);


        $rules = array(
            'name'          => 'required',
            'date'          => 'required|data'
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {
            $data['date'] = Carbon::createFromFormat( 'd/m/Y', $data['date']);
            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];
            $data['gallery_id'] = hashid_decode($data['gallery_id'])[0];

            $gallery_post = GalleryPost::find( hashid_decode($id) )->first();
            $gallery_post->update($data);

            if(isset($data['gallery_id'])){
                $gallery = Gallery::find($data['gallery_id']);
            }else{
                $gallery = new Gallery();
            }

            $gallery->images = $images;
            $gallery->save();

            $gallery_post->gallery_id = $gallery->id;
            $gallery_post->save();

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'O galeria de fotos foi atualizada.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => 'Não foi possível atualizar.'
            ], 500 );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try{

            $id = hashid_decode($id);
            $post = GalleryPost::find($id)->first(); //first pq o hashid é array
            $gallery = Gallery::find($post->gallery_id);

            if($gallery instanceof Gallery){
                $gallery->delete();
            }
            $post->delete();

            DB::commit();

            return response( [
                'code' => 1,
                'message' => 'Registro removido.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();

            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
