<?php

namespace App\Http\Controllers\Api;

use App\Models\Gallery;
use App\Models\TourSpot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class TourSpotController extends Controller
{

    public function store( Request $request ) {

        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );

        $rules = array(
            'title'         => 'required',
            'info'          => 'required',
            'type'          => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];

            $tourspot = TourSpot::create( $data );

            $gallery = new Gallery();
            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $tourspot->gallery_id = $gallery->id;
            $tourspot->save();

            DB::commit();
            return response( $tourspot->toJson(), 200 );

        } catch ( ValidationException $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }

    }

    public function update( Request $request, $id ) {

        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );

        $rules = array(
            'title'         => 'required',
            'info'          => 'required',
            'type'          => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];
            $data['gallery_id'] = hashid_decode($data['gallery_id'])[0];

            $tourspot = TourSpot::find( hashid_decode($id) )->first();
            $tourspot->update($data);

            if(isset($data['gallery_id'])){
                $gallery = Gallery::find($data['gallery_id']);
            }else{
                $gallery = new Gallery();
            }

            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $tourspot->gallery_id = $gallery->id;
            $tourspot->save();

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'O evento foi salvo.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }

    }

    public function destroy( $id ) {

        DB::beginTransaction();

        try{

            $id = hashid_decode($id);
            $tourspot = TourSpot::find($id)->first();
            $gallery = Gallery::find($tourspot->gallery_id);

            if($gallery instanceof Gallery){
                $gallery->delete();
            }
            $tourspot->delete();

            DB::commit();
            return response( [
                'code' => 1,
                'message' => 'Registro removido.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();

            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
