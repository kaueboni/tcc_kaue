<?php

namespace App\Http\Controllers\Api;

use App\Models\CategoryDefault;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryDefaultController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data   = $request->input();

        $rules = array(
            'name'          => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        if(CategoryDefault::where('name',$data['name'])->get()->count()>0){
            return response( [
                'code'    => 11,
                'message' => 'Já existe uma categoria com esse nome.'
            ], 500 );
        }

        $data['level'] = 1;
        $data['slug'] = str_slug($data['name']);

        DB::beginTransaction();
        try {

            $category = CategoryDefault::create( $data );

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A categoria foi adicionada.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();
            return response( [
                'code'    => 12,
                'message' => 'Não foi possível salvar.'
            ], 500 );
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data   = $request->input();

        $rules = array(
            'name'          => 'required',
            'parent_id'     => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        $cat = CategoryDefault::where('name',$data['name'])->first();

        if($cat instanceof CategoryDefault &&
           $cat->id =! hashid_decode($data['category_id'])[0])
        {
            return response( [
                'code'    => 11,
                'message' => 'Já existe uma categoria com esse nome.'
            ], 500 );
        }

        $data['level'] = 2;
        $data['slug'] = str_slug($data['name']);

        DB::beginTransaction();
        try {

            $category = CategoryDefault::find(hashid_decode($id)[0]);
            $category->update($data);

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A categoria foi atualizada.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();
            return response( [
                'code'    => 12,
                'message' => 'Não foi possível salvar.'
            ], 500 );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try{
            $id = hashid_decode($id);
            $cat = CategoryDefault::find($id)->first(); //first pq o hashid é array
            $cat->delete();

            DB::commit();
            return response( [
                'code' => 1,
                'message' => 'Categoria removida.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();
            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
