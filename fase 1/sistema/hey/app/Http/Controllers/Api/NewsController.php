<?php

namespace App\Http\Controllers\Api;

use App\Models\Gallery;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class NewsController extends Controller
{


    public function store(Request $request)
    {
        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );


        $rules = array(
            'title'       => 'required',
            'subtitle'   => 'required',
            'content'    => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];

            $news    = News::create( $data );

            $gallery = new Gallery();
            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $news->gallery_id = $gallery->id;
            $news->save();

            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A notícia foi salva.'
            ], 200 );

        } catch ( ValidationException $e ) {

            DB::rollBack();
            return response( [
                'code'    => 11,
                'message' => $e
            ], 500 );
        }
    }

    public function update(Request $request, $id)
    {
        $data   = $request->input( 'formData' );
        $images = $request->input( 'gallery' );

        $rules = array(
            'title'      => 'required|max:180',
            'subtitle'   => 'required|max:180',
            'content'    => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 10,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $data['franchisee_id'] = hashid_decode( $data['franchisee_id'] )[0];
            $data['gallery_id'] = hashid_decode($data['gallery_id'])[0];

            $news = News::find(hashid_decode($id))->first();
            $news->update($data);

            if(isset($data['gallery_id'])){
                $gallery = Gallery::find($data['gallery_id']);
            }else{
                $gallery = new Gallery();
            }

            $gallery->images = serialize($images); //serializando a galeria (unserialize)
            $gallery->save();

            $news->gallery_id = $gallery->id;
            $news->save();


            DB::commit();
            return response( [
                'code'    => 1,
                'message' => 'A notícia foi atualizada.'
            ], 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();
            return response( [
                'code'    => 11,
                'message' => 'Não foi possível atualizar a notícia.'
            ], 500 );
        }
    }



    public function destroy($id)
    {
        DB::beginTransaction();

        try{

            $id = hashid_decode($id);
            $news = News::find($id)->first(); //first pq o hashid é array
            $gallery = Gallery::find($news->gallery_id);

            if($gallery instanceof Gallery){
                $gallery->delete();
            }
            $news->delete();

            DB::commit();

            return response( [
                'code' => 1,
                'message' => 'Registro removido.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();

            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }
    }
}
