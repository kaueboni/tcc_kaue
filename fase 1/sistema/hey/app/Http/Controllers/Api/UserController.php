<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    public function update( Request $request ) {

        $data = $request->input();

        $user = User::find(hashid_decode($data['id']))->first();

        if ( $data['email'] != $user->email ) {
            if ( User::where( 'email', $data['email'] )->get()->count() > 0 ) {
                return response( [
                    'code'    => 10,
                    'message' => 'Já existe um usuário com este e-mail.'
                ], 500 );
            }
        }

        $rules = array(
            'name'      => 'required',
            'cpf'       => 'required|cpf',
            'rg'        => 'required',
            'birthdate' => 'required|data',
            'email'     => 'required',
            'phone'     => 'required|telefone_com_ddd',
            'cellphone' => 'required|celular_com_ddd',

        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code'    => 11,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        try{

            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->cpf = $data['cpf'];
            $user->rg = $data['rg'];
            $user->phone = $data['phone'];
            $user->cellphone = $data['cellphone'];
            $user->birthdate = Carbon::createFromFormat('d/m/Y',$data['birthdate']);
            $user->personal_info = 1;
            $user->save();

            return response( [
                'code'    => $user->level,
                'message' => 'As informações do usuário foram atualizadas.'
            ], 200 );


        }catch(\Exception $e){
            return response( [
                'code'    => 12,
                'message' => $e
            ], 500 );
        }


    }


}
