<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FranchiseeCandidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FranchiseeCandidateController extends Controller {


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request ) {

        $data = $request->input();

        if(FranchiseeCandidate::where('owner_email', $data['owner_email'])->get()->count() > 0){
            return response( [
                'code' => 12,
                'message' => 'Já existe uma solicitação para este e-mail.'
            ], 500 );
        }

        $rules = array(
            'owner_name'      => 'required',
            'owner_phone'     => 'required|telefone_com_ddd',
            'owner_cellphone' => 'required|celular_com_ddd',
            'owner_email'     => 'required|email',
            'city_name'       => 'required',
            'city_state'      => 'required',
        );

        $validation = Validator::make( $data, $rules );

        if ( $validation->fails() == 'true' ) {
            return response( [
                'code' => 11,
                'message' => $validation->errors()->first()
            ], 500 );
        }

        DB::beginTransaction();
        try {

            $franchisee_candidate = FranchiseeCandidate::create( $data );


            DB::commit();

            return response( $franchisee_candidate->toJson(), 200 );

        } catch ( \Exception $e ) {

            DB::rollBack();

            return response( [
                'code' => 10,
                'message' => 'Algo deu errado. Não foi possível enviar os dados.'
            ], 500 );

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id ) {

        DB::beginTransaction();

        try{

            $id = hashid_decode($id);
            $candidate = FranchiseeCandidate::find($id)->first(); //first pq o hashid é array
            $candidate->delete();

            DB::commit();

            return response( [
                'code' => 1,
                'message' => 'Registro removido.'
            ], 200 );

        }catch(\Exception $e){

            DB::rollBack();

            return response( [
                'code' => 10,
                'message' => 'Não foi possível remover.'
            ], 500 );
        }

    }
}
