<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Franchisee;
use App\Models\Plan;

class CityController extends Controller {

    public function home( $slug ) {

        $franchisee = Franchisee::where( 'slug', $slug )->first();
        $categories_menu = $franchisee->categories;
        $companies  = $franchisee->companies;
        $news       = $franchisee->news->first();
        $tourspot   = $franchisee->tourspots->first();
        $event      = $franchisee->events->first();
        $gallery    = $franchisee->galleries->first();
        $plans      = Plan::orderBy( 'id', 'desc' )->take( 3 )->get();

        return view( 'site.city.home', compact( [
            'event',
            'news',
            'gallery',
            'tourspot',
            'plans',
            'franchisee',
            'categories_menu'
        ] ) );
    }

    public function empresas( $city, $slug ) {

        $franchisee = Franchisee::where( 'slug', $city )->first();
        $categories_menu = $franchisee->categories;
        $category   = $franchisee->categories->where( 'slug', $slug );
        $category   = $category->first();
        $companies  = $category->companies;

        return view( 'site.companies.archive', compact( [ 'companies', 'franchisee', 'category','categories_menu' ] ) );
    }

    public function empresa( $city, $id ) {

        $franchisee = Franchisee::where( 'slug', $city )->first();
        $categories_menu = $franchisee->categories;
        $company = Company::find( $id );
        $image = '';

        if(isset($company->gallery))
            $images  = unserialize( $company->gallery );

        return view( 'site.companies.single', compact( 'company', 'images','franchisee','categories_menu' ) );

    }
}


