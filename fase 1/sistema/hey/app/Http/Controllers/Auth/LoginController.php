<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    protected function authenticated( Request $request, $user ) {

        switch ( $user->level ) {

            case 1 :
                $data = [ 'code' => 1, 'message' => 'Usuário logado como empresa.' ];
                break;

            case 2 :
                $data = [ 'code' => 2, 'message' => 'Usuário logado como franqueado.' ];
                break;

            case 3 :
                $data = [ 'code' => 3, 'message' => 'Usuário logado como admin.' ];
                break;
        }



        return response( $data, 200 );

    }


    /**
     * Override laravel method to verify user
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login( Request $request ) {


        // dd($request->input());
        $this->validateLogin( $request );


        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ( $this->hasTooManyLoginAttempts( $request ) ) {
            $this->fireLockoutEvent( $request );

            return $this->sendLockoutResponse( $request );
        }

        if ( $this->attemptLogin( $request ) ) {

            /*$user = User::where( 'email', $request->input( 'email' ) )->first();

            if ( $user->verified == 0 ) {
                return $this->sendFailedLoginResponse( $request );
            }*/

            return $this->sendLoginResponse( $request );
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts( $request );

        return $this->sendFailedLoginResponse( $request );
    }

    public function logout(  ) {
        if(auth()->user()){
            $user_email = auth()->user()->email;
            Auth::logout();
            return redirect()->route('login');
        } else {
            return response('Não há nenhum usuário logado no sistema.',200);
        }

    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo(){

    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
