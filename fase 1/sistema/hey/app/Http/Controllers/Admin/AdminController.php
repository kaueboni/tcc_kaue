<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\CategoryDefault;
use App\Models\Company;
use App\Models\Franchisee;
use App\Models\FranchiseeCandidate;
use App\Models\Plan;

class AdminController extends Controller
{
    public function dashboard(){
        $companies = Company::all()->count();
        $candidates = Company::all()->count();
        return view('admin.dashboard',compact(['companies']));

    }

    public function financial(){
        return view('admin.financial');
    }

    public function personalInfo(){
        return view('personal_info');
    }

    public function franchisees(){

        $candidates = FranchiseeCandidate::all();
        $franchisees = Franchisee::all();

        return view('admin.franchisees.list')
            ->with('candidates',$candidates)
            ->with('franchisees',$franchisees);
    }

    public function companies(){
        $companies = Company::all();
        return view('admin.companies.list')
            ->with('companies',$companies);
    }

    public function editFranchisee(){
        return view('admin.franchisees.form');
    }

    public function plans(){
        $plans = Plan::all();
        return view('admin.plans.list')
            ->with('plans',$plans);
    }

    public function createPlan(){
        return view('admin.plans.form');
    }

    public function editPlan($id){
        try{
            $id = hashid_decode($id);
            $plan = Plan::find($id)->first();

            return view('admin.plans.form')
                ->with('plan',$plan);

        }catch(\Exception $e){
            return response( [
                'code' => 10,
                'message' => 'Não foi possível recuperar.'
            ], 500 );
        }
    }

    public function categories(){
        $categories = CategoryDefault::all();
        return view('admin.categories.list')
            ->with('categories',$categories);
    }

    public function createCategory(){
        $categories = CategoryDefault::where('level',1)->get();
        return view('admin.categories.form')
            ->with('categories',$categories);
    }

    public function editCategory($id){

        try{
            $id = hashid_decode($id);
            $category = CategoryDefault::find($id)->first();
            $categories = CategoryDefault::where('level',1)->get();

            return view('admin.categories.form')
                ->with('category',$category)
                ->with('categories',$categories);

        }catch(\Exception $e){
            return response( [
                'code' => 10,
                'message' => 'Não foi possível recuperar.'
            ], 500 );
        }
    }
}
