<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Gallery;

class CompanyController extends Controller {

    public function dashboard() {
        return view( 'company.dashboard' );
    }

    public function edit() {
        $company = auth()->user()->company;
        $categories = $company->franchisee->categories;
        $company_cats = $company->categories->pluck('id');
        return view( 'company.form', compact([
            'company',
            'categories',
            'company_cats',
        ]));
    }
}
