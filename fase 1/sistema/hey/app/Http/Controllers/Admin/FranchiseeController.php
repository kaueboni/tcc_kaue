<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CompanyCandidate;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\GalleryPost;
use App\Models\News;
use App\Models\TourSpot;

class FranchiseeController extends Controller {

    public function dashboard() {
        return view( 'franchisee.dashboard' );
    }

    public function login() {
        return view( 'franchisee.login' );
    }

    public function financial() {
        return view( 'franchisee.financial' );
    }

    public function companies() {
        $candidates = CompanyCandidate::all();
        $companies = auth()->user()->franchisee->companies;
        return view( 'franchisee.companies.list' )
            ->with('candidates',$candidates)
            ->with('companies',$companies);
    }

    public function createCompany() {
        return view( 'franchisee.companies.form' );
    }

    public function editCompany( $id ) {
        return view( 'franchisee.companies.form' );
    }

    public function events() {
        $events = Event::all();

        return view( 'franchisee.events.list' )
            ->with( 'events', $events );
    }

    public function createEvent() {

        return view( 'franchisee.events.form' );

    }

    public function editEvent( $id ) {

        try {
            $id      = hashid_decode( $id );
            $event   = Event::find( $id )->first();
            $gallery = Gallery::find( $event->gallery_id );

            return view( 'franchisee.events.form' )
                ->with( 'event', $event )
                ->with( 'gallery', $gallery );

        } catch ( \Exception $e ) {
            return response( [
                'code'    => 10,
                'message' => 'Não foi possível recuperar.'
            ], 500 );
        }


    }

    public function tourSpots() {
        $tourspots = TourSpot::where('franchisee_id',auth()->user()->franchisee->id)->get();
        $galleries_ids = $tourspots->pluck('gallery_id');
        $images = Gallery::whereIn('id',$galleries_ids)->get();
        return view( 'franchisee.tourspots.list' )
            ->with('tourspots',$tourspots)
            ->with('images',$images);
    }

    public function createTourSpot() {
        return view( 'franchisee.tourspots.form' );
    }

    public function editTourSpot( $id ) {
        $id      = hashid_decode( $id );
        $tourspot   = TourSpot::find( $id )->first();
        $gallery = Gallery::find( $tourspot->gallery_id );
        return view( 'franchisee.tourspots.form' )
            ->with( 'tourspot', $tourspot )
            ->with( 'gallery', $gallery );
    }

    public function news() {
        $allNews = News::where('franchisee_id',auth()->user()->franchisee->id)->get();
        $galleries_ids = $allNews->pluck('gallery_id');
        $images = Gallery::whereIn('id',$galleries_ids)->get();
        return view( 'franchisee.news.list' )
            ->with('all_news',$allNews)
            ->with('images',$images);
    }

    public function createNew() {
        return view( 'franchisee.news.form' );
    }

    public function editNew( $id ) {
        $news = News::find(hashid_decode($id))->first();
        $gallery = Gallery::find($news->gallery_id);
        return view( 'franchisee.news.form' )
            ->with('news',$news)
            ->with('gallery',$gallery);
    }

    public function galleries() {
        $galleries = GalleryPost::all();
        $images = Gallery::all();
        return view( 'franchisee.galleries.list')
            ->with('galleries', $galleries)
            ->with('images', $images);
    }

    public function createGallery() {
        return view( 'franchisee.galleries.form' );
    }

    public function editGallery( $id ) {
        try {
            $id           = hashid_decode( $id );
            $gallery_post = GalleryPost::find( $id )->first();
            $gallery      = Gallery::find( $gallery_post->gallery_id );

            return view( 'franchisee.galleries.form' )
                ->with( 'gallery_post', $gallery_post )
                ->with( 'gallery', $gallery );

        } catch ( \Exception $e ) {
            return response( [
                'code'    => 10,
                'message' => 'Não foi possível recuperar.'
            ], 500 );
        }
    }

    public function categories() {
        if ( auth()->user()->franchisee ) {
            $franchisee = auth()->user()->franchisee;
            $categories = $franchisee->categories;
        } else {
            return route( 'login' );
        }
        $categories = $categories->where( 'level', 2 );
        return view( 'franchisee.categories.list' )
            ->with( 'categories', $categories );
    }

    public function createCategory() {
        if ( auth()->user()->franchisee ) {
            $franchisee = auth()->user()->franchisee;
            $categories = $franchisee->categories;
        } else {
            return route( 'login' );
        }
        $categories = $categories->where( 'level', 1 );

        return view( 'franchisee.categories.form' )
            ->with( 'categories', $categories );
    }

    public function editCategory( $id ) {

        $category = Category::find(hashid_decode($id))->first();

        if ( auth()->user()->franchisee ) {
            $franchisee = auth()->user()->franchisee;
            $categories = $franchisee->categories;
        } else {
            return route( 'login' );
        }
        $categories = $categories->where( 'level', 1 );

        return view( 'franchisee.categories.form' )
            ->with( 'categories', $categories )
            ->with('category', $category);
    }


}
