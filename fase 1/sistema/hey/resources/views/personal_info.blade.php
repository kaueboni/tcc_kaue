@extends('admin.layouts.main-tpl')

@section('title', 'Meus dados')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        @if(auth()->user()->personal_info < 1)
        <div class="alert alert-warning" role="alert">
           Você precisa atualizar suas informações antes de prosseguir!
        </div>
        @endif
        <div class="row">
            <h3 class="text-center">Dados pessoais</h3>
        </div>
        <hr>
        <form id="formUserInfo">
            <input type="hidden" name="id" value="{{ hashid_encode(auth()->user()->id)}}">
            @csrf
            <div class="d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="">Seu nome</label>
                        <input class="form-control" type="text" value="{{ auth()->user()->name }}" name="name">
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">CPF</label>
                                <input class="form-control cpf" type="text" value="{{ auth()->user()->cpf }}" name="cpf">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">RG</label>
                                <input class="form-control" type="text" value="{{ auth()->user()->rg }}" name="rg">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Data de Nascimento</label>
                                <input class="form-control date" type="text" value="{{ \Carbon\Carbon::parse(auth()->user()->birthdate)->format('d/m/Y') }}" name="birthdate">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">E-mail</label>
                                <input class="form-control" type="text" value="{{ auth()->user()->email }}" name="email">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Telefone</label>
                                <input class="form-control phone_with_ddd" type="text" value="{{ auth()->user()->phone }}" name="phone">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="">Celular/WhatsApp</label>
                                <input class="form-control cel_with_ddd" type="text" value="{{ auth()->user()->cellphone }}" name="cellphone">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex-center">
                <button class="btn btn-md btn-primary" type="submit">Atualizar</button>
                <a href=""><button class="btn btn-md btn-light" type="submit">Cancelar</button></a>
            </div>
        </form>
    </div>
@endsection
