@extends('site.layouts.main-tpl')

@section('title', 'HeyCidades - Presidente Epitácio')

@section('content')

    <section class="jumbotron card wow fadeIn"
             style="background: linear-gradient(to left, #fe8c00, #f83600);">

        <!-- Content -->
        <div class="card-body text-white text-center py-5 px-5 my-5">

            <h1 class="mb-4">
                <strong>Bem vindo ao HeyCidades {{ $franchisee->city_name }}</strong>
            </h1>
            <p>
            <h3 class="mb-4"><strong>Encontre as melhores opções de culinária, hospedagem, turismo e muito mais...</strong></h3>
            </p>
            <div class="form-group">
                <input class="form-control form-control-lg" type="text" placeholder="O que você Procura">
            </div>

        </div>
        <!-- Content -->
    </section>
    <hr>

    <section>
        <div class="row">
            <div class="col-lg-4 col-md-12">

                <!--Image-->
                <div class="view overlay rounded z-depth-1-half mb-3">
                    <img src="https://www.eatthis.com/content/uploads/media/images/ext/179383586/Perkins-Restaurant-Burger-500x300.jpg" class="img-fluid"
                         alt="Sample post image">
                    <a>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <h3 class="text-center">
                    <a>
                        <strong>Onde comer</strong>
                    </a>
                </h3>
            </div>
            <div class="col-lg-4 col-md-12">

                <!--Image-->
                <div class="view overlay rounded z-depth-1-half mb-3">
                    <img src="http://www.reservehotelonline.com.br/resources/img/br/campina-grande/hotel-village-premium-campina-grande.jpg" class="img-fluid"
                         alt="Sample post image">
                    <a>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <h3 class="text-center">
                    <a>
                        <strong>Onde ficar</strong>
                    </a>
                </h3>
            </div>
            <div class="col-lg-4 col-md-12">

                <!--Image-->
                <div class="view overlay rounded z-depth-1-half mb-3">
                    <img src="http://nordesterural.com.br/wp-content/uploads/2018/02/enoturismo-uruguai-inverno-500x300.jpg" class="img-fluid"
                         alt="Sample post image">
                    <a>
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>

                <!--Excerpt-->
                <h3 class="text-center">
                    <a>
                        <strong>Onde ir</strong>
                    </a>
                </h3>
            </div>
        </div>
    </section>

    <hr>

    <!--Section: Magazine v.1-->
    <section class="wow fadeIn">
        <div class="container-fluid">
            <!--Grid row-->
            <div class="row text-left">


                @if(isset($event))
                    <?php
                    $images = \App\Models\Gallery::find($event->gallery_id);
                    $images = unserialize($images->images);
                    ?>
                <div class="col-lg-3 col-md-12">

                    <!--Image-->
                    <div class="view overlay rounded z-depth-1-half mb-3">
                        <img src="{{ $images[0] }}" class="img-thumbnail"
                             alt="Sample post image">
                        <a>
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>

                    <!--Excerpt-->
                    <div class="news-data">
                        <a href="" class="light-blue-text">
                            <h6>
                                <i class="fa fa-rss"></i>
                                <strong> Eventos</strong>
                            </h6>
                        </a>
                        <p>
                            <strong>
                                <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($event->date)->format('d/m/Y')}}</strong>
                        </p>
                    </div>
                    <h3>
                        <a>
                            <strong>{{ $event->name }}</strong>
                        </a>
                    </h3>


                </div>
                @else
                    <div class="col-lg-3 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="http://via.placeholder.com/300x300" class="img-thumbnail"
                                 alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-rss"></i>
                                    <strong>Posts</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <i class="fa fa-clock-o"></i> 20/08/2018</strong>
                            </p>
                        </div>
                        <h3>
                            <a>
                                <strong>Post exemplo</strong>
                            </a>
                        </h3>
                    </div>
                @endif

                @if(isset($news))
                    <?php
                    $images = \App\Models\Gallery::find($news->gallery_id);
                    $images = unserialize($images->images);
                    ?>
                    <div class="col-lg-3 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="{{ $images[0] }}" class="img-thumbnail"
                                 alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-rss"></i>
                                    <strong> Notícias</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($event->date)->format('d/m/Y')}}</strong>
                            </p>
                        </div>
                        <h5>
                            <a>
                                <strong>{{ $news->title }}</strong>
                            </a>
                        </h5>


                    </div>
                @else
                        <div class="col-lg-3 col-md-12">

                            <!--Image-->
                            <div class="view overlay rounded z-depth-1-half mb-3">
                                <img src="http://via.placeholder.com/300x300" class="img-thumbnail"
                                     alt="Sample post image">
                                <a>
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>

                            <!--Excerpt-->
                            <div class="news-data">
                                <a href="" class="light-blue-text">
                                    <h6>
                                        <i class="fa fa-rss"></i>
                                        <strong>Post</strong>
                                    </h6>
                                </a>
                                <p>
                                    <strong>
                                        <i class="fa fa-clock-o"></i> 20/08/2018</strong>
                                </p>
                            </div>
                            <h3>
                                <a>
                                    <strong>Post exemplo</strong>
                                </a>
                            </h3>
                        </div>
                @endif

                @if(isset($gallery))
                    <?php
                    $images = \App\Models\Gallery::find($gallery->gallery_id);
                    $images = unserialize($images->images);
                    ?>
                    <div class="col-lg-3 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="{{ $images[0] }}" class="img-thumbnail"
                                 alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-rss"></i>
                                    <strong> Galerias de Fotos</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($gallery->date)->format('d/m/Y')}}</strong>
                            </p>
                        </div>
                        <h5>
                            <a>
                                <strong>{{ $gallery->name }}</strong>
                            </a>
                        </h5>


                    </div>
                @else
                        <div class="col-lg-3 col-md-12">

                            <!--Image-->
                            <div class="view overlay rounded z-depth-1-half mb-3">
                                <img src="http://via.placeholder.com/300x300" class="img-thumbnail"
                                     alt="Sample post image">
                                <a>
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                            </div>

                            <!--Excerpt-->
                            <div class="news-data">
                                <a href="" class="light-blue-text">
                                    <h6>
                                        <i class="fa fa-rss"></i>
                                        <strong>Post</strong>
                                    </h6>
                                </a>
                                <p>
                                    <strong>
                                        <i class="fa fa-clock-o"></i> 20/08/2018</strong>
                                </p>
                            </div>
                            <h3>
                                <a>
                                    <strong>Post exemplo</strong>
                                </a>
                            </h3>
                        </div>
                @endif

                @if(isset($tourspot))
                    <?php
                    $images = \App\Models\Gallery::find($tourspot->gallery_id);
                    $images = unserialize($images->images);
                    ?>
                    <div class="col-lg-3 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="{{ $images[0] }}" class="img-thumbnail"
                                 alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-rss"></i>
                                    <strong> Galerias de Fotos</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <?php $tourspot->type == 1 ? $type = 'Público' : $type = 'Privado' ?>
                                    <i class="fa fa-clock-o"></i> {{ $type }}</strong>
                            </p>
                        </div>
                        <h5>
                            <a>
                                <strong>{{ $tourspot->title }}</strong>
                            </a>
                        </h5>


                    </div>
                @else
                    <div class="col-lg-3 col-md-12">

                        <!--Image-->
                        <div class="view overlay rounded z-depth-1-half mb-3">
                            <img src="http://via.placeholder.com/300x300" class="img-thumbnail"
                                 alt="Sample post image">
                            <a>
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!--Excerpt-->
                        <div class="news-data">
                            <a href="" class="light-blue-text">
                                <h6>
                                    <i class="fa fa-rss"></i>
                                    <strong>Post</strong>
                                </h6>
                            </a>
                            <p>
                                <strong>
                                    <i class="fa fa-clock-o"></i> 20/08/2018</strong>
                            </p>
                        </div>
                        <h3>
                            <a>
                                <strong>Post exemplo</strong>
                            </a>
                        </h3>
                    </div>
                @endif






            </div>
            <!--Grid row-->
        </div>
        <br>
    </section>
    <!--/Section: Magazine v.1-->

    <hr>
    @if(isset($plans))
    <section class="wow fadeIn">

        <div class="container-fluid">

            <h2 class="my-5 h3 text-center">Quer colocar sua empresa no HeyCidades</h2>
{{--
            <h5 class="my-5 h5 text-center">In a professional context it often happens that private or corporate clients corder a publication to be made and presented with the actual content still not being ready. </h5>
--}}

            <!--Grid row-->
            <div class="row text-center wow fadeIn">

                @foreach($plans as $plan)
                <div class="col-lg-4 col-md-12 mb-4">
                    <!--Card-->
                    <div class="card">

                        <!-- Card header -->
                        <div class="card-header">
                            <h4>
                                <strong>{{ $plan->name }}</strong>
                            </h4>
                        </div>

                        <!--Card content-->
                        <div class="card-body">

                            <ol class="list-unstyled mb-4">
                                {!! $plan->description !!}
                            </ol>

                            <button type="button" class="btn btn-lg btn-block btn-primary waves-effect" data-toggle="modal" data-target="#companyModal">Cadastrar
                            </button>

                        </div>

                    </div>
                    <!--/.Card-->
                </div>
                @endforeach


            </div>
            <!--Grid row-->

        </div>

    </section>
    @endif
    <!-- Modal -->
    <div class="modal fade" id="franchiseeModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="transferModalLabel">Seja um franqueado</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="container-fluid">
                    <div class="modal-body">
                        <h5 class="card-title">Preencha o formulário e entraremos em contato</h5>
                    </div>
                    <form id="franchiseeSignUpForm">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="exampleForm2">Seu nome</label>
                                    <input type="text" class="form-control" name="owner_name" >
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="exampleForm2">Telefone</label>
                                    <input type="text"  class="phone_with_ddd form-control" name="owner_phone">
                                </div>
                                <div class="col-lg-6">
                                    <label for="exampleForm2">WhatsApp</label>
                                    <input type="text"  class="cel_with_ddd form-control" name="owner_cellphone">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-8">
                                    <label for="exampleForm2">Email</label>
                                    <input type="email" class="form-control" name="owner_email">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-8">
                                    <label for="exampleForm2">Cidade</label>
                                    <input type="text" class="form-control" name="city_name">
                                </div>
                                <div class="col-lg-4">
                                    <label for="exampleForm2">Estado</label>
                                    <select class="custom-select" name="city_state">
                                        <option value="SP">SP</option>
                                        <option value="MG">MG</option>
                                        <option value="RJ">SP</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-success">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="companyModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="transferModalLabel">Contrate um plano para a sua empresa</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="container-fluid">
                    <div class="modal-body">
                        <h5 class="card-title">Preencha o formulário, entraremos em contato para validação dos seus dados
                        após isso você receberá em seu e-mail uma senha para que possa logar em nosso sistema, editar
                        as informações de sua empresa e contratar um plano.</h5>
                    </div>
                    <form id="companySignUpForm">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <label for="company_name">Nome da empresa</label>
                                    <input type="text" name="company_name" id="company_name" class="form-control">
                                    <input type="hidden" name="franchisee_id" value="{{ $franchisee->id }}">
                                </div>
                                <div class="col-lg-4">
                                    <label for="primary_phone">Telefone primário</label>
                                    <input type="text" name="primary_phone" id="primary_phone" class="phone_with_ddd form-control">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-8">
                                    <label for="street_name">Endereço</label>
                                    <input type="text" name="street_name" id="street_name" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="street_number">Número</label>
                                    <input type="text" name="street_number" id="street_number" class="form-control">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="complement">Complemento</label>
                                    <input type="text" name="complement" id="complement" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="district">Bairro</label>
                                    <input type="text" name="district" id="district" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="zipcode">CEP</label>
                                    <input type="text" name="zipcode" id="zipcode" class="form-control cep">
                                </div>
                            </div>
                            <br>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label for="owner_name">Seu nome</label>
                                    <input type="text" name="owner_name" id="owner_name" class="form-control">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="owner_phone">Telefone</label>
                                    <input type="text" name="owner_phone" id="owner_phone" class="phone_with_ddd form-control">
                                </div>
                                <div class="col-lg-6">
                                    <label for="owner_cellphone">WhatsApp</label>
                                    <input type="text" name="owner_cellphone" id="owner_cellphone" class="cel_with_ddd form-control">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-8">
                                    <label for="owner_email">Email</label>
                                    <input type="email" name="owner_email" id="company_name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-success">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@endsection


