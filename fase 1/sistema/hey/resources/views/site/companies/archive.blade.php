@extends('site.layouts.company-tpl')

@section('title', 'HeyCidades - Presidente Epitácio')

@section('content')

    <section class="wow fadeIn" style="margin-top: 80px">
        <h3>{{ $category->name }} em {{ $franchisee->city_name }}</h3>
        <hr>
        <div class="container-fluid">
            <div class="row">

                    @foreach($companies as $company)
                        <?php
                            if(isset($company->gallery))
                                $images = unserialize($company->gallery);
                            if(is_array($images))
                                $image = $images[0]
                        ?>
                    <div class="col-lg-3 company-card">
                        <div class="card">
                            <img class="card-img-top" src="{{ $image }}" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title"><a>{{ $company->name }}</a></h4>
                                <p class="card-text">{!! str_limit($company->summary, $limit = 100, $end = '...') !!}</p>
                                <a href="{{ route('company',['city'=> $franchisee->slug, 'company'=>$company->id]) }}" class="btn btn-primary">Ver detalhes</a>
                            </div>
                        </div>
                    </div>
                    @endforeach


            </div>


        </div>
        <br>
    </section>








@endsection


