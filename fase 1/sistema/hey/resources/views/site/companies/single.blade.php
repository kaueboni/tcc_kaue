@extends('site.layouts.company-tpl')

@section('title', 'HeyCidades - '. $company->name)

@section('content')

    <div class="container-fluid pt-5 mt-lg-5">
        <section class="first-section">
            <div class="row">
                <div class="col-lg-6">
                    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                        <!--Indicators-->
                    {{--<ol class="carousel-indicators">
                        <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                        <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                    </ol>--}}
                    <!--/.Indicators-->
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">

                            <!--First slide-->
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ $images[0] }}" alt="First slide">
                            </div>
                            <!--/First slide-->
                            <!--Second slide-->


                            @foreach($images as $image)
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{ $image }}" alt="First slide">
                                </div>
                        @endforeach
                        <!--/Third slide-->
                        </div>
                        <!--/.Slides-->
                        <!--Controls-->
                        <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        <!--/.Controls-->
                    </div>
                </div>
                <div class="col-lg-6">
                    <h1 class="title">{{ $company->name }}</h1>
                    <h5 class="phones">{{ $company->summary }}</h7>
                        <hr>
                        <div class="row">
                            <div class="col-lg-5">
                                @if(isset($company->primary_phone))

                                    <h3><i class="material-icons text-primary">phone</i>{{ $company->primary_phone }}
                                    </h3>
                                @endif
                                @if(isset($company->secondary_phone))
                                    <h3><i class="material-icons text-primary">phone</i>{{ $company->primary_phone }}
                                    </h3>
                                @endif
                                @if(isset($company->cellphone))
                                    <h3><i class="material-icons text-primary">phone</i>{{ $company->cellphone }}</h3>
                                @endif
                            </div>
                            <div class="col-lg-7">
                                @if(isset($company->email))
                                    <h4><i class="material-icons text-primary">mail</i>{{ $company->email }}</h4>
                                @endif
                                @if(isset($company->street_name)&&isset($company->street_number))
                                    <h4>
                                        <i class="material-icons text-primary">mail</i>{{ $company->street_name . ', '.$company->street_number }}
                                    </h4>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Descrição Detalhada</h3>
                                {!! $company->description or "" !!}
                            </div>
                        </div>
                </div>
            </div>
        </section>

        <hr>

        <section>

            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <div class="google-map" style="height: 390px"></div>
                    <input type="hidden" id="latitude" name="latitude"
                           value="{{ $company->latitude or '-21.766494' }}">
                    <input type="hidden" id="longitude" name="longitude"
                           value="{{ $company->longitude or '-52.1155447' }}">
                </div>
            </div>
            {{--<div class="col-lg-4 col-md-4 mb-4">
                <div class="form-group">
                    <label for="">ID Google Places (Caso a empresa já tenha se cadastrado no Google)</label>
                    <input class="form-control" type="text" value="ChIJN1t_tDeuEmsRUsoyG83frY4" disabled>
                </div>
            </div>--}}


        </section>

        <hr>
        <section>

            {!! $company->menu or "" !!}

        </section>


    </div>







@endsection


