<!--Main Navigation-->
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light primary-color-dark scrolling-navbar">
        <div class="container-fluid">

            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="/{{ $franchisee->slug }}">
                <strong class="blue-text">Hey Cidades</strong>
                <small class="text-white">{{ $franchisee->city_name }}</small>
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                    @foreach($categories_menu as $category)
                        @if($category->level == 1)
                            @if($category->childrens->count() > 0)
                                <?php $childrens = $category->childrens ?>

                                <li class="nav-item">
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $category->name }}</a>

                                        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                            @foreach($childrens as $child)
                                                <a class="dropdown-item" href="{{ route('category',['city'=>$franchisee->slug,'category'=>$child->slug]) }}">{{$child->name}}</a>
                                            @endforeach
                                        </div>

                                </li>
                            @else
                            <li class="nav-item">
                                <a class="nav-link waves-effect text-white" href="{{ route('category',['city'=>$franchisee->slug,'category'=>$category->slug]) }}">{{ $category->name }}
                                </a>
                            </li>
                            @endif
                        @endif
                    @endforeach


                </ul>
            </div>

        </div>
    </nav>
    <!-- Navbar -->


</header>
<!--Main Navigation-->