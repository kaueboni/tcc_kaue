<!DOCTYPE html>
<html>
    <head>

        <title>{{ config('app.name') }} - @yield('title')</title>

        @include('site.layouts.head')

    </head>

    <body>

    @include('site.layouts.header')

        <main class="mx-lg-5">
            @yield('content')
        </main>

    @include('site.layouts.footer')

  {{--  @include('admin.layouts.footer')--}}
    @include('site.layouts.requires')

    </body>
