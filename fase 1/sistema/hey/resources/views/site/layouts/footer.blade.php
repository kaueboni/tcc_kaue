<!-- Footer -->
<footer class="page-footer font-small blue pt-4 mt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <div class="col-md-4 mt-md-0 mt-3 text-center">
            </div>

            <!-- Grid column -->
            <div class="col-md-4 mt-md-0 mt-3 text-center">
                <div class="news">
                    <h4>Seja um franqueado...</h4>
                    <p class="text-white">Leve o HeyCidades para a sua cidade, cresça com a gente!</p>
                    <a href="#" class="btn btn-md btn-info" data-toggle="modal" data-target="#franchiseeModal">Saiba mais</a>
                </div>
            </div>
            <!-- Grid column -->

            <div class="col-md-4 mt-md-0 mt-3 text-center">
            </div>


        </div>
        <!-- Grid row -->

    </div>
    <br>

    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3"><a href="https://www.facebook.com/mdbootstrap" target="_blank">
            <i class="fa fa-facebook mr-3"></i>
        </a>

        <a href="https://twitter.com/MDBootstrap" target="_blank">
            <i class="fa fa-twitter mr-3"></i>
        </a>

        <a href="https://www.youtube.com/watch?v=7MUISDJ5ZZ4" target="_blank">
            <i class="fa fa-youtube mr-3"></i>
        </a>

        <a href="https://plus.google.com/u/0/b/107863090883699620484" target="_blank">
            <i class="fa fa-google-plus mr-3"></i>
        </a>

        <a href="https://dribbble.com/mdbootstrap" target="_blank">
            <i class="fa fa-dribbble mr-3"></i>
        </a>

        <a href="https://pinterest.com/mdbootstrap" target="_blank">
            <i class="fa fa-pinterest mr-3"></i>
        </a>

        <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
            <i class="fa fa-github mr-3"></i>
        </a>

        <a href="http://codepen.io/mdbootstrap/" target="_blank">
            <i class="fa fa-codepen mr-3"></i>
        </a>
        © 2018 Copyright:
        <a href="https://mdbootstrap.com/bootstrap-tutorial/"> MDBootstrap.com</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->