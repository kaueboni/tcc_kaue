@extends('admin.layouts.main-tpl')

@section('title', 'Empresas')

@section('content')

    @include('company.components.table')

@endsection
