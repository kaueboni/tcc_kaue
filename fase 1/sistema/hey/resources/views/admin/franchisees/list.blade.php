@extends('admin.layouts.main-tpl')

@section('title', 'Franqueados')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Franqueados</h3>
                </div>
            </div>
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-general-tab" data-toggle="tab" href="#general" role="tab"
                           aria-controls="nav-home" aria-selected="true">Todos</a>
                        <a class="nav-item nav-link" id="nav-assent-tab" data-toggle="tab" href="#assent" role="tab"
                           aria-controls="nav-profile" aria-selected="false">Em aprovação</a>
                    </div>
                </nav>
                <br>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table id="companies_table" class="datatable table table-striped" style="width:100%">
                            @if($franchisees->count() > 0)
                                <thead>
                                <tr>
                                    <th>Franqueado</th>
                                    <th>Cidade</th>
                                    <th>Estado</th>
                                    <th>Empresas</th>
                                    <th>Planos ativos</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($franchisees as $franchisee)
                                    <tr>
                                        <td>{{ $franchisee->user->name }}</td>
                                        <td>{{ $franchisee->city_name }}</td>
                                        <td>{{ $franchisee->city_state }}</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>
                                            <a href="{{route('city',$franchisee->slug)}}"><i class="material-icons md-24 text-info">remove_red_eye</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr> Ainda não há nenhum franqueado em aprovação. </tr>
                                @endif
                                </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="assent" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table id="companies_table" class="datatable table table-striped" style="width:100%">
                            @if($candidates->count() > 0)
                            <thead>
                            <tr>
                                <th>Nome do Franqueado</th>
                                <th>Telefone</th>
                                <th>Cel./Whats</th>
                                <th>E-mail</th>
                                <th>Cidade</th>
                                <th>UF</th>
                                <th>Solicitado em</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($candidates as $candidate)
                                <tr>
                                    <td>{{ $candidate->owner_name }}</td>
                                    <td class="phone_with_ddd">{{ $candidate->owner_phone }}</td>
                                    <td>{{ $candidate->owner_cellphone }}</td>
                                    <td>{{ $candidate->owner_email }}</td>
                                    <td>{{ $candidate->city_name }}</td>
                                    <td>{{ $candidate->city_state }}</td>
                                    <td>{{ \Carbon\Carbon::parse($candidate->created_at)->format('d/m/Y') }}</td>
                                    <td>
                                        <a class="candidate-remove" candidate-id="{{ hashid_encode($candidate->id) }}" href="#"><i class="material-icons md-24 text-danger">close</i></a>
                                        <a class="franchisee-candidate-accept" candidate-id="{{ hashid_encode($candidate->id) }}" href="#"><i class="material-icons md-24 text-info">check</i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                            <tr> Ainda não há nenhum franqueado em aprovação. </tr>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
