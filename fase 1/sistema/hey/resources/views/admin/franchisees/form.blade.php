@extends('admin.layouts.main-tpl')

@section('title', 'Novo franqueado')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-center">Novo franqueado</h3>
        </div>
        <hr>
        <div class="d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Nome do Franqueado</label>
                    <input class="form-control" type="text" value="" v-model="companyForm.name">
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">CPF</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">RG</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Data de Nascimento</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">E-mail</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Telefone</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">Cidade</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Categoria mãe</label>
                            <select class="custom-select" name="uf" id="">
                                <option value="1">SP</option>
                                <option value="2">MG</option>
                                <option value="3">PR</option>
                                <option value="4">RS</option>
                                <option value="4">SC</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-center">
            <button class="btn btn-md btn-success" type="submit">Salvar</button>
            <button class="btn btn-md btn-primary" type="submit">Atualizar</button>
            <a href="/admin/franqueados"><button class="btn btn-md btn-light" type="submit">Cancelar</button></a>
        </div>
    </div>
@endsection
