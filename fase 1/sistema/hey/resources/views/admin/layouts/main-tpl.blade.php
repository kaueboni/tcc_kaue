<!DOCTYPE html>
<html>
    <head>

        <title>{{ config('app.name') }} - @yield('title')</title>

        @include('admin.layouts.head')

    </head>

    <body>

    @include('admin.layouts.header')

        <main class="mx-lg-5">
            @yield('content')
        </main>

  {{--  @include('admin.layouts.footer')--}}
    @include('admin.layouts.requires')

    </body>
