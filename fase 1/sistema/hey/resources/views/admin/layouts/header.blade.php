<!--Main Navigation-->
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light primary-color-dark scrolling-navbar">
        <div class="container-fluid">
        <?php
            switch(auth()->user()->level){
                case 1 :
                    $label = 'Empresa';
                    break;
                case 2 :
                    $label = 'Franqueado';
                    break;
                case 3 :
                    $label = 'Admin';
                    break;
            }
        ?>


            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="{{url('/'.strtolower($label))}}">
                <strong class="blue-text">Hey Cidades</strong> <small class="text-white">{{ $label }}</small>
            </a>

            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left -->
                <ul class="navbar-nav mr-auto">
                    @if(auth()->user()->personal_info == 1)

                        <li class="nav-item">
                            <a class="nav-link waves-effect text-white" href="{{ url('/admin') }}">Principal
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect text-white" href="{{ url('/admin/financeiro') }}">Financeiro</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect text-white" href="{{ url('/admin/planos') }}">Planos</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect text-white" href="{{ url('/admin/franqueados') }}"">Franqueados</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect text-white" href="{{ url('/admin/categorias') }}"">Categorias</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect text-white" href="{{ url('/admin/empresas') }}">Empresas</a>
                        </li>

                    @endif
                </ul>

                <!-- Right -->
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/dados-pessoais">Meus dados</a>
                            <a class="dropdown-item" href="/logout">Logout</a>
                        </div>
                    </li>
                </ul>

            </div>

        </div>
    </nav>
    <!-- Navbar -->



</header>
<!--Main Navigation-->