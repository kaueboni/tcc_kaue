@extends('admin.layouts.main-tpl')

@section('title', 'Categorias')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Categorias</h3>
                    <a  href="/admin/categorias/add" ><button class="btn btn-sm btn-info">Add Nova</button></a>
                </div>
            </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($categories->count() > 0)
                        <thead>
                        <tr>
                            <th class="text-center">Nome</th>
                            <th class="text-center">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $cat)
                            <tr>
                                <td>{{ $cat->name }}</td>


                                <td style="width: 130px">
                                    <a class="cat-default-remove" id="{{ hashid_encode($cat->id) }}" href="#"><i class="material-icons md-24 text-danger">close</i></a>
                                    <a class="cat-default-edit" id="{{ hashid_encode($cat->id) }}" href="{{ '/admin/categorias/'. hashid_encode($cat->id) }}"><i class="material-icons md-24 text-info">remove_red_eye</i></a>
                                </td>
                            </tr>
                        @endforeach
                </table>
                @else
                    <tr> Ainda não há nenhuma categoria cadastrada. </tr>
                @endif
            </div>
        </div>
    </div>

@endsection
