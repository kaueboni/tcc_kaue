@extends('admin.layouts.main-tpl')

@section('title', 'Categoria')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-center">Nova Categoria</h3>
        </div>

        <hr>
        <form id="formCategoryDefault">
            <div class="d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="">Nome da Categoria</label>
                        @if(isset($category))
                        <input type="hidden" name="category_id" value="{{ hashid_encode($category->id) }}">
                        @endif
                        <input class="form-control" type="text" name=name value="{{ $category->name or "" }}">
                    </div>
                </div>
            </div>
            <div class="flex-center">
                @if(!isset($category))
                <button class="btn btn-md btn-success" type="submit">Salvar</button>
                @else
                <button class="btn btn-md btn-primary btn-update" type="submit">Atualizar</button>
                @endif
                <a href="/admin/categorias"><button class="btn btn-md btn-light" type="submit">Cancelar</button></a>
            </div>
        </form>
    </div>
@endsection
