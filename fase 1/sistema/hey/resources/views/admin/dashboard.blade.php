@extends('admin.layouts.main-tpl')

@section('title', 'Principal')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">

        <br>
        <h1>Olá, Nome do Admin</h1>
        <h5>Confira um balanço geral da plataforma.</h5>
        <br>

        <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

            <!--Grid column-->
            <div class="col-lg-3 col-md-12 mb-4">
                <!--Card-->
                <a href="admin/empresas">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Novas Empresas</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>178</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>132</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>
            <!--Grid column-->



            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4">
                <!--Card-->
                <a href="/admin/franqueados">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Franqueados em aprovação</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>4</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>8</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>
            <!--Grid column-->

            <div class="col-lg-3 col-md-6 mb-4">
                <!--Card-->
                <a href="/admin/financeiro">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Planos vendidos</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>54</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>37</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4">

                <!--Card-->
                <a href="/admin/financeiro">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Financeiro</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Saldo disponível</h7><br>
                            <h2>R$ 4.852,57</h2><br>
                            <h7>Lançamentos futuros</h7><br>
                            <h2>R$ 3.895,69</h2>
                        </div>
                    </div>
                </a>

                <!--/.Card-->

            </div>
            <!--Grid column-->

        </div>

    </div>






@endsection
