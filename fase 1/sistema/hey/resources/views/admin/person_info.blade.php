@extends('admin.layouts.main-tpl')

@section('title', 'Meus dados')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-center">Dados pessoais</h3>
        </div>
        <hr>
        <div class="d-flex justify-content-center">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Seu nome</label>
                    <input class="form-control" type="text" value="" v-model="companyForm.name">
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">CPF</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">RG</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Data de Nascimento</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="">E-mail</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Telefone</label>
                            <input class="form-control" type="text" value="" v-model="companyForm.name">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-center">
            <button class="btn btn-md btn-success" type="submit">Salvar</button>
            <button class="btn btn-md btn-primary" type="submit">Atualizar</button>
            <a href="/admin"><button class="btn btn-md btn-light" type="submit">Cancelar</button></a>
        </div>
    </div>
@endsection
