@extends('admin.layouts.main-tpl')

@section('title', 'Financeiro')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">

        <br>
        <h1>Financeiro</h1>
        <h5>Acompanhe as movimentações financeiras da plataforma.</h5>
        <br>
        <div class="row" style="visibility: visible; animation-name: fadeIn;">
            <!--Grid column-->
            <div class="col-lg-3 col-md-4 mb-4 text-center">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Saldo disponível</h5>
                        <!-- Title -->
                        <h1 class="card-title ">R$ 4.852,57</h1>
                        <!-- Buttons -->
                        <a href="#" class="btn btn-md btn-info" data-toggle="modal" data-target="#transferModal">Solicitar Transferência</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 mb-4 text-center">
                <div class="card ">
                    <div class="card-body">
                        <h5 class="card-title">Lançamentos futuros</h5>
                        <!-- Title -->
                        <h1 class="card-title ">R$ 3.895,69</h1>
                        <!-- Button -->
{{--
                        <a href="#" class="btn btn-md btn-info">Detalhes</a>
--}}
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 text-center">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Em análise</h5>
                        <!-- Title -->
                        <h1 class="card-title ">R$ 1.581,05</h1>
                        <!-- Button -->
                        {{--<a href="#" class="btn btn-md btn-info">Detalhes</a>--}}
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 mb-4 text-center">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Recebido</h5>
                        <!-- Title -->
                        <h1 class="card-title ">R$ 1.581,05</h1>
                        <!-- Button -->
{{--
                        <a href="#" class="btn btn-md btn-info">Detalhes</a>
--}}
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-general-tab" data-toggle="tab" href="#all" role="tab"
                   aria-controls="nav-home" aria-selected="true">Todos lançamentos</a>
                <a class="nav-item nav-link" id="nav-assent-tab" data-toggle="tab" href="#future" role="tab"
                   aria-controls="nav-profile" aria-selected="false">Futuros</a>
                <a class="nav-item nav-link" id="nav-assent-tab" data-toggle="tab" href="#assent" role="tab"
                   aria-controls="nav-profile" aria-selected="false">Em análise</a>
                <a class="nav-item nav-link" id="nav-assent-tab" data-toggle="tab" href="#received" role="tab"
                   aria-controls="nav-profile" aria-selected="false">Recebidos</a>

            </div>
        </nav>
        <br>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="nav-home-tab">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Cliente</th>
                        <th>Tipo do Plano</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th>Atualizado em</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Nosso Hotel</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Pago</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pousada do Ciro</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Disponível</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Hotel Gold Medal</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Recebido</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pizzaria Portuguesa</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Em análise</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Ribas Lanchonete</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Cancelado</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="future" role="tabpanel" aria-labelledby="nav-profile-tab">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Cliente</th>
                        <th>Tipo do Plano</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th>Atualizado em</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Nosso Hotel</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Pago</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pousada do Ciro</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Disponível</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Hotel Gold Medal</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Recebido</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pizzaria Portuguesa</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Em análise</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Ribas Lanchonete</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Cancelado</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="assent" role="tabpanel" aria-labelledby="nav-profile-tab">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Cliente</th>
                        <th>Tipo do Plano</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th>Atualizado em</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Nosso Hotel</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Pago</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pousada do Ciro</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Disponível</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Hotel Gold Medal</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Recebido</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pizzaria Portuguesa</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Em análise</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Ribas Lanchonete</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Cancelado</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="received" role="tabpanel" aria-labelledby="nav-profile-tab">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Cliente</th>
                        <th>Tipo do Plano</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th>Atualizado em</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Nosso Hotel</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Pago</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pousada do Ciro</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Disponível</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Hotel Gold Medal</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Recebido</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Pizzaria Portuguesa</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Em análise</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    <tr>
                        <th>PL-KJA1412AZ1</th>
                        <td>Ribas Lanchonete</td>
                        <td>Ouro</td>
                        <td>R$ 215,00</td>
                        <td>Cancelado</td>
                        <td>16/05/2018 às 20:45</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <br>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="transferModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transferModalLabel">Transferência de Saldo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5 class="card-title">Saldo disponível</h5>
                    <!-- Title -->
                    <h1 class="card-title ">R$ 4.852,57</h1>
                    <!--  -->
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-8">
                            <label for="exampleForm2">Numero da Agencia</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                        <div class="col-lg-2">
                            <label for="exampleForm2">Dígito</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-lg-4">
                            <label for="exampleForm2">Nº do banco</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                        <div class="col-lg-6">
                            <label for="exampleForm2">Numero da Conta</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                        <div class="col-lg-2">
                            <label for="exampleForm2">Digito</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="exampleForm2">Nome do Portador da Conta</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6">
                            <label for="exampleForm2">CPF</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                        <div class="col-lg-6">
                            <label for="exampleForm2">RG</label>
                            <input type="text" id="exampleForm2" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-success">Transferir</button>
                </div>
            </div>
        </div>
    </div>








@endsection
