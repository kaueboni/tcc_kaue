@extends('admin.layouts.main-tpl')

@section('title', 'Planos')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Planos</h3>
                    <a  href="/admin/planos/add" ><button class="btn btn-sm btn-info">Add Novo</button></a>
                </div>
                 </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($plans->count() > 0)
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th width="150">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($plans as $plan)
                            <tr>
                                <td>{{ $plan->name}}</td>
                                <td>{!! $plan->description !!}</td>
                                <td>R$ <span class="money">{{ $plan->price}}</span></td>

                                <td>
                                    <a class="plan-remove" id="{{ hashid_encode($plan->id) }}" href="#"><i class="material-icons md-24 text-danger">close</i></a>
                                    <a class="plan-edit" href="{{ '/admin/planos/'. hashid_encode($plan->id) }}"><i class="material-icons md-24 text-info">mode_edit</i></a>
                                </td>
                            </tr>
                         @endforeach
                    @else
                            <tr> Ainda não há nenhum plano cadastrado aprovação. </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
