@extends('admin.layouts.main-tpl')

@section('title', 'Planos')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-center">Novo Plano</h3>
        </div>
        <hr>
        <form id="formPlan">
            <div class="d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="">Nome do Plano</label>
                        <input class="form-control" type="text" value="{{ $plan->name or "" }}" name="name">
                        @if(isset($plan))<input type="hidden" name="plan_id" value="{{ hashid_encode($plan->id) }}">@endif
                    </div>
                    <div class="form-group">
                        <label for="">Descrição
                         </label>
                        <textarea rows="4" class="form-control" type="text" name="description">{!! $plan->description or "" !!}</textarea>
                    </div>

                    <div class="form-group" style="width: 50%">
                        <label for="">Valor</label>
                        <input class="form-control money" type="text" value="{{ $plan->price or "" }}" name="price">
                    </div>
                </div>
            </div>
            <div class="flex-center">
                @if(!isset($plan))
                <button class="btn btn-md btn-success" type="submit">Salvar</button>
                @else
                <button class="btn btn-md btn-primary" type="submit">Atualizar</button>
                @endif
                <a href="/admin/planos"><button class="btn btn-md btn-light" type="submit">Cancelar</button></a>
            </div>
        </form>
    </div>
@endsection
