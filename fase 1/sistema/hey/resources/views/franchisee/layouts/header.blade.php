<!--Main Navigation-->
<header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light primary-color-dark scrolling-navbar">
        <div class="container-fluid">

            <!-- Brand -->
            <a class="navbar-brand waves-effect" href="{{url('/franqueado')}}">
                <strong class="blue-text">HeyCidades</strong> <small class="text-white">Franqueado</small>
            </a>

            <!-- Collapse -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Links -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Left -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link waves-effect text-white" href="{{ url('/franqueado') }}">Principal
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cidade</a>
                            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="{{ url('/franqueado/eventos') }}">Eventos</a>
                                <a class="dropdown-item" href="{{ url('/franqueado/galerias') }}">Galerias de Fotos</a>
                                <a class="dropdown-item" href="{{ url('/franqueado/noticias')}}">Notícias</a>
                                <a class="dropdown-item" href="{{ url('/franqueado/pontos-turisticos') }}">Pontos Turísticos</a>
                            </div>
                        </li>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link waves-effect text-white" href="{{ url('/franqueado/empresas') }}">Empresas</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link waves-effect text-white" href="{{ url('/franqueado/categorias') }}">Categorias</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link waves-effect text-white" href="{{ url('/franqueado/financeiro') }}">Financeiro</a>
                    </li>

                </ul>

                <!-- Right -->
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/dados-pessoais">Meus dados</a>
                            <a class="dropdown-item" href="/logout">Logout</a>
                        </div>
                    </li>
                </ul>

            </div>

        </div>
    </nav>
    <!-- Navbar -->



</header>
<!--Main Navigation-->