@extends('franchisee.layouts.main-tpl')

@section('title', 'Categorias')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Categorias</h3>
                    <a href="/franqueado/categorias/add">
                        <button class="btn btn-sm btn-info">Add Nova</button>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($categories->count() > 0)
                        <thead>
                        <tr>
                            <th class="text-center">Nome</th>
                            <th class="text-center">Nível</th>
                            <th class="text-center">Categoria Mãe</th>
                            <th class="text-center">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $cat)
                            <tr>
                                <td>{{ $cat->name }}</td>
                                <td class="text-center">{{ $cat->level }}</td>
                                <td class="text-center">{{ $cat->parent->name or "" }}</td>
                                <td style="width: 130px">

                                   <a class="btn btn-sm btn-danger fa fa-remove category-remove"
                                       id="{{ hashid_encode($cat->id) }}" href="#"></a>
                                    <a class="btn btn-sm btn-warning fa fa-pencil category-edit"
                                       id="{{ hashid_encode($cat->id) }}"
                                       href="{{ '/franqueado/categorias/'. hashid_encode($cat->id) }}"></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr> Ainda não há nenhuma categoria cadastrada.</tr>
                    @endif
                </table>
            </div>
        </div>
    </div>

@endsection
