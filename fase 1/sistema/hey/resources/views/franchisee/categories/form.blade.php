@extends('franchisee.layouts.main-tpl')

@section('title', 'Categoria')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-center">Nova Categoria</h3>
            <a href="/franqueado/categorias">
                <button class="btn btn-sm btn-info">Ver todas</button>
            </a>
        </div>

        <hr>
        <form id="formCategory">
            <div class="d-flex justify-content-center">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="">Nome da Categoria</label>
                        @if(isset($category))
                            <input type="hidden" name="category_id" value="{{ hashid_encode($category->id) }}">
                        @endif
                        @if(isset(auth()->user()->franchisee->id))
                            <input type="hidden" name="franchisee_id" value="{{ auth()->user()->franchisee->id }}">
                        @endif
                        <input class="form-control" type="text" name="name" value="{{ $category->name or "" }}">
                    </div>
                    <div class="form-group">
                        <label for="">Categoria mãe</label>
                        <select class="custom-select" name="parent_id">
                            <option disabled>Selecione uma categoria mãe</option>
                            @foreach($categories as $cat)
                                <option value={{ $cat->id }}>{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="flex-center">
                @if(!isset($category))
                    <button class="btn btn-md btn-success" type="submit">Salvar</button>
                @else
                    <button class="btn btn-md btn-primary btn-update" type="submit">Atualizar</button>
                @endif
                <a href="/franqueado/categorias"><button class="btn btn-md btn-light" type="submit">Cancelar</button></a>
            </div>
        </form>
    </div>
@endsection
