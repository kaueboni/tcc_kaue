@extends('franchisee.layouts.main-tpl')

@section('title', 'Eventos')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Eventos</h3>
                    <a  href="/franqueado/eventos/add" ><button class="btn btn-sm btn-info">Add Novo</button></a>
                </div>
            </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($events->count() > 0)
                        <thead>
                        <tr>
                            <th>Nome do Evento</th>
                            <th>Data</th>
                            <th>Hora</th>
                            <th>Local</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr>
                                <td>{{ $event->name }}</td>
                                <td>{{ \Carbon\Carbon::parse($event->date)->format('d/m/Y') }}</td>
                                <td>{{ \Carbon\Carbon::parse($event->date)->format('H:i')}}</td>
                                <td>{{ $event->location_name }}</td>
                                <td style="width: 130px">
                                    <a class="btn btn-sm btn-danger fa fa-remove event-remove" id="{{ hashid_encode($event->id) }}" href="#"></a>
                                    <a class="btn btn-sm btn-warning fa fa-pencil event-edit" id="{{ hashid_encode($event->id) }}" href="{{ '/franqueado/eventos/'. hashid_encode($event->id) }}"></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr> Ainda não há nenhum franqueado em aprovação. </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
