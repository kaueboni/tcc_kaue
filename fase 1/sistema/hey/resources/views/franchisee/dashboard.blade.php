@extends('franchisee.layouts.main-tpl')

@section('title', 'Area do Franqueado')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">

        <br>
        <h1>Olá, {{ auth()->user()->name }}</h1>
        <h5>Confira um balanço geral da sua franquia HeyCidades.</h5>
        <br>

        <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

            <!--Grid column-->
            <div class="col-lg-3 col-md-12 mb-4">
                <!--Card-->
                <a href="">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Novas Empresas</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>54</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>39</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4">
                <!--Card-->
                <a href="">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Planos vendidos</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>54</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>37</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4">
                <!--Card-->
                <a href="">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Empresas em aprovação</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>56</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>35</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-lg-3 col-md-6 mb-4">

                <!--Card-->
                <a href="{{url('franqueado/financeiro')}}">
                    <div class="card">

                        <!-- Card header -->
                        <div class="card-header">Conta da plataforma</div>

                        <!--Card content-->
                        <div class="card-body">
                            <h7>Saldo disponível</h7><br>
                            <h2>R$ 4.852,57</h2><br>
                            <h7>Lançamentos futuros</h7><br>
                            <h2>R$ 3.895,69</h2>
                        </div>

                    </div>
                </a>

                <!--/.Card-->

            </div>
            <!--Grid column-->

        </div>

    </div>






@endsection
