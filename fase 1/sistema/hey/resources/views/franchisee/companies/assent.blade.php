@extends('franchisee.layouts.main-tpl')

@section('title', 'Cidades')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">

            <!--Card content-->
            <div class="card-body d-sm-flex justify-content-between">

                <h4 class="mb-2 mb-sm-0 pt-1">
                    Empresas em aprovação
                </h4>

                <form class="d-flex justify-content-center">
                    <!-- Default input -->
                    <input placeholder="Pesquisar" aria-label="Search" class="form-control" type="search">
                    <button class="btn btn-primary btn-sm my-0 p waves-effect waves-light" type="submit">
                        <i class="fa fa-search"></i>
                    </button>

                </form>

            </div>

        </div>

        <div class="card mb-4">
            <div class="card-body">
                <!-- Table  -->
                <table class="table table-hover">
                    <!-- Table head -->
                    <thead class="grey lighten-5">
                    <tr>
                        <th>#</th>
                        <th>Cidade</th>
                        <th>Franqueado</th>
                        <th>Taxa</th>
                        <th>Nº de Empresas</th>
                        <th>Nº de Planos</th>
                        <th>R$ ??</th>
                    </tr>
                    </thead>
                    <!-- Table head -->

                    <!-- Table body -->
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Presidente Epitácio</td>
                        <td>Arlindo Cruz</td>
                        <td>20%</td>
                        <td>237</td>
                        <td>89</td>
                        <td>R$ 1.920,12</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Maringá</td>
                        <td>Juca Kfouri</td>
                        <td>18%</td>
                        <td>237</td>
                        <td>89</td>
                        <td>R$ 1.920,12</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Assis</td>
                        <td>Antonio Carlos Medeiros</td>
                        <td>15%</td>
                        <td>237</td>
                        <td>89</td>
                        <td>R$ 1.920,12</td>
                    </tr>
                    </tbody>
                    <!-- Table body -->
                </table>
                <!-- Table  -->

            </div>
        </div>


    </div>






@endsection
