@extends('franchisee.layouts.main-tpl')

@section('title', 'Empresa')

@section('content')

   @include('company.components.form')

@endsection
