@extends('franchisee.layouts.main-tpl')

@section('title', 'Empresas em aprovação')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Empresas</h3>
                </div>
            </div>
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-general-tab" data-toggle="tab" href="#general" role="tab"
                           aria-controls="nav-home" aria-selected="true">Todos</a>
                        <a class="nav-item nav-link" id="nav-assent-tab" data-toggle="tab" href="#assent" role="tab"
                           aria-controls="nav-profile" aria-selected="false">Em aprovação</a>
                    </div>
                </nav>
                <br>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="nav-profile-tab">
                        @if(isset($companies)&&$companies->count() > 0)
                        <table id="companies_table" class="datatable table table-striped" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Nome do Responsável</th>
                                    <th>Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($companies as $company)
                                    <tr>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->user->name }}</td>
                                        <td>
                                            <a class="company-remove" company-id="{{ hashid_encode($company->id) }}" href="#"><i class="material-icons md-24 text-danger">close</i></a>
                                            <a href="{{route('company',['city'=>$company->franchisee->slug,'company'=>$company->id])}}"><i class="material-icons md-24 text-info">remove_red_eye</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                        </table>
                        @else
                            <p> Ainda não há registros. </p>
                        @endif

                    </div>

                    <div class="tab-pane fade" id="assent" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table id="companies_table" class="datatable table table-striped" style="width:100%">
                            @if($candidates->count() > 0)
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Endereço</th>
                                        <th>Telefone primário</th>
                                        <th>Nome do Responsável</th>
                                        <th>Celular do Responsável</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($candidates as $candidate)
                                    <tr>
                                        <td>{{ $candidate->company_name }}</td>
                                        <td>{{ $candidate->street_name . ', '. $candidate->street_number }}</td>
                                        <td class="phone_with_ddd">{{ $candidate->owner_cellphone }}</td>
                                        <td>{{ $candidate->owner_name }}</td>
                                        <td class="cel_with_ddd">{{ $candidate->owner_cellphone }}</td>
                                        <td>
                                            <a class="company-candidate-remove" candidate-id="{{ hashid_encode($candidate->id) }}" href="#"><i class="material-icons md-24 text-danger">close</i></a>
                                            <a class="candidate-accept" candidate-id="{{ hashid_encode($candidate->id) }}" href="#"><i class="material-icons md-24 text-success">check</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                        </table>
                        @else
                            <p> Ainda não há registros. </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
