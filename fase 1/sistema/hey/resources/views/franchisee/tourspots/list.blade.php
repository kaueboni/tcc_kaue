@extends('franchisee.layouts.main-tpl')

@section('title', 'Pontos Turísticos')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Pontos turísticos</h3>
                    <a  href="/franqueado/pontos-turisticos/add" ><button class="btn btn-sm btn-info">Add Novo</button></a>
                </div>
            </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($tourspots->count() > 0)
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Tipo</th>
                            <th>Imagens</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tourspots as $tourspot)
                            <?php
                            $gallery = $images->where('id',$tourspot->gallery_id)->first();
                            $tourspot->type == 2 ?
                                $type = 'Privado' :
                                $type = 'Público';
                            ?>
                            <tr>
                                <td>{{ $tourspot->title }}</td>
                                <td>{{ $type }}</td>
                                <td>
                                    @if(isset($gallery->images)&&is_array(unserialize($gallery->images)))
                                        @foreach(unserialize($gallery->images) as $image)
                                            <img width="50" height="50" class="mr-3 mb-2 preview-img" src="{{ $image }}" alt="Generic placeholder image">
                                        @endforeach
                                    @else
                                        <small class="text-muted">Não possui imagens</small>
                                    @endif
                                </td>
                                <td style="width: 130px">
                                    <a alt="Remover" class="tourspot-remove" id="{{ hashid_encode($tourspot->id) }}" href="#"><i class="material-icons md-24 text-danger">close</i></a>
                                    <a alt="Editar" id="{{ hashid_encode($tourspot->id) }}" href="{{ '/franqueado/pontos-turisticos/'. hashid_encode($tourspot->id) }}"><i class="material-icons md-24 text-info">edit</i></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr> Ainda não há nenhum registro. </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
