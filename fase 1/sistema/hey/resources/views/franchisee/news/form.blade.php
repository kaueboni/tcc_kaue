@extends('franchisee.layouts.main-tpl')

@section('title', 'Notícia')

@section('content')

    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-left">@if(isset($news)) Editar notícia @else Nova notícia @endif</h3>
            <a  href="/franqueado/noticias" ><button class="btn btn-sm btn-info">Ver todas</button></a>
        </div>
        <hr>
        <form id="formNews">
            <div class="tab-content" id="nav-tabContent">
                <div class="row">
                    <div class="col-lg-4 col-md-4 mb-4">
                        <div class="form-group">
                            <label for="">Título</label>
                            <input class="form-control" type="text" name="title" value="{{ $news->title or "" }}">
                            @if(isset($news))<input type="hidden" name="news_id" value="{{ hashid_encode($news->id) }}" >@endif
                            <input type="hidden" name="franchisee_id" value={{ hashid_encode(auth()->user()->franchisee->id) }}>
                            @if(isset($gallery))
                                <input type="hidden" name="gallery_id" value={{ hashid_encode($gallery->id) }}>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-4 mb-4">
                        <div class="form-group">
                            <label for="">Subtítulo</label>
                            <input class="form-control" type="text" name="subtitle" value="{{ $news->subtitle or "" }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-2 mb-2">
                        <textarea id="tinyEditor" name="content" style="height: 350px">{{ $news->content or "" }}</textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <!-- Our markup, the important part here! -->
                        <div id="drag-and-drop-zone" class="dm-uploader p-5" style="height: 250px">
                            <h3 class="mb-5 mt-5 text-center">Arraste e solte imagens</h3>
                            <div class="btn btn-primary btn-sm" style="margin-top: -50px">
                                <span>Selecione no browser</span>
                                <input type="file" title='Clique para adicionar uma imagem'/>
                            </div>
                        </div><!-- /uploader -->
                    </div>
                    <div class="col-md-6 col-sm-12" style="height: 250px">
                        <div class="card h-100">
                            <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                @if(isset($gallery)&&is_array(unserialize($gallery->images)))
                                    @foreach(unserialize($gallery->images) as $image)
                                        <li class="media" id="uploaderFileztulb6q1hk" path="{{ $image }}" gallery_id="{{ $gallery->id }}">
                                            <img width="50" height="50" class="mr-3 mb-2 preview-img" src="{{ $image }}" alt="Generic placeholder image">
                                            <div class="media-body mb-1">
                                                <p class="mb-2">
                                                    <strong>Imagem</strong> - Status: <span class="status text-success">Salva</span>
                                                </p>
                                                <div class="progress mb-2">
                                                    <div class="progress-bar bg-primary bg- bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
                                                    <a href="#" class="text-danger"> Remover </a>
                                                </div>
                                                <hr class="mt-1 mb-1">
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li class="text-muted text-center empty">Não há imagens ainda.</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="flex-center">
                @if(isset($news))
                    <button class="btn btn-md btn-primary btn-update" type="submit">Atualizar</button>
                @else
                    <button class="btn btn-md btn-success" type="submit">Salvar</button>
                @endif
                    <a href="/franqueado/noticias" class="btn btn-light btn-sm">Cancelar</a>
            </div>
            <br>
            <br>
        </form>
    </div>



    <script type="text/html" id="files-template">
        <li class="media">
            <div class="media-body mb-1">
                <p class="mb-2">
                    <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
                </p>
                <div class="progress mb-2">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                         role="progressbar"
                         style="width: 0%"
                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                    </div>
                    <a href="#" class="text-danger"> Remover </a>
                </div>
                <hr class="mt-1 mb-1" />
            </div>
        </li>
    </script>



@endsection