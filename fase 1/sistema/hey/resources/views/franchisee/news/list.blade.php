@extends('franchisee.layouts.main-tpl')

@section('title', 'Notícias')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Noticias</h3>
                    <a  href="/franqueado/noticias/add" ><button class="btn btn-sm btn-info">Add Nova</button></a>
                </div>
            </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($all_news->count() > 0)
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Subtítulo</th>
                            <th>Imagens</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($all_news as $news)
                            <?php
                            $gallery = $images->where('id',$news->gallery_id)->first();
                            ?>
                            <tr>
                                <td>{{ $news->title }}</td>
                                <td>{{ $news->subtitle }}</td>
                                <td>
                                    @if(isset($gallery->images)&&is_array(unserialize($gallery->images)))
                                        @foreach(unserialize($gallery->images) as $image)
                                            <img width="50" height="50" class="mr-3 mb-2 preview-img" src="{{ $image }}" alt="Generic placeholder image">
                                        @endforeach
                                    @else
                                    <small class="text-muted">Não possui imagens</small>
                                        @endif
                                </td>
                                <td style="width: 130px">
                                    <a class="btn btn-sm btn-danger fa fa-remove news-remove" id="{{ hashid_encode($news->id) }}" href="#"></a>
                                    <a class="btn btn-sm btn-warning fa fa-pencil " id="{{ hashid_encode($news->id) }}" href="{{ '/franqueado/noticias/'. hashid_encode($news->id) }}"></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr> Ainda não há nenhum registro. </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
