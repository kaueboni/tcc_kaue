@extends('franchisee.layouts.main-tpl')

@section('title', 'Fotos de Eventos')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">
        <div class="card mb-4">
            <div class="card-header">
                <div class="row">
                    <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Galerias de Fotos</h3>
                    <a  href="/franqueado/galerias/add" ><button class="btn btn-sm btn-info">Add Nova</button></a>
                </div>
            </div>
            <div class="card-body">
                <table id="companies_table" class="datatable table table-striped" style="width:100%">
                    @if($galleries->count() > 0)
                        <thead>
                        <tr>
                            <th>Nome da Galeria de Fotos</th>
                            <th>Imagens</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($galleries as $gallery_post)
                            <?php
                            $gallery = $images->where('id',$gallery_post->gallery_id)->first();
                            ?>
                            <tr>
                                <td>{{ $gallery_post->name }}</td>
                                <td>
                                    @if(isset($gallery->images)&&is_array(unserialize($gallery->images)))
                                        @foreach(unserialize($gallery->images) as $image)
                                            <img width="50" height="50" class="mr-3 mb-2 preview-img" src="{{ $image }}" alt="Generic placeholder image">
                                        @endforeach
                                    @endif
                                </td>
                                <td>{{ \Carbon\Carbon::parse($gallery_post->date)->format('d/m/Y') }}</td>
                                <td style="width: 130px">
                                    <a class="btn btn-sm btn-danger fa fa-remove gallery-remove" id="{{ hashid_encode($gallery_post->id) }}" href="#"></a>
                                    <a class="btn btn-sm btn-warning fa fa-pencil gallery-edit" id="{{ hashid_encode($gallery_post->id) }}" href="{{ '/franqueado/galerias/'. hashid_encode($gallery_post->id) }}"></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr> Ainda não há nenhum franqueado em aprovação. </tr>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
