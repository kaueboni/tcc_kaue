@extends('franchisee.layouts.main-tpl')

@section('title', 'Fotos de evento')

@section('content')

    <div class="container-fluid pt-5 mt-lg-5">
        <div class="row">
            <h3 class="text-left">@if(isset($gallery_post)) Editar Galeria @else Nova Galeria @endif </h3>
            <a  href="/franqueado/galerias" ><button class="btn btn-sm btn-info">Ver todas</button></a>
        </div>
        <hr>
        <form id="formGallery">
            <div class="tab-content" id="nav-tabContent">
                <div class="row">
                    <div class="col-lg-6 col-md-4 mb-4">
                        <div class="form-group">
                            <label for="">Nome da Galeria</label>
                            <input class="form-control" type="text" value="{{ $gallery_post->name or "" }}" name="name">
                            @if(isset($gallery_post))<input type="hidden" name="gallery_post_id" value="{{ hashid_encode($gallery_post->id) }}" >@endif
                            <input type="hidden" name="franchisee_id" value={{ hashid_encode(auth()->user()->franchisee->id) }}>
                            @if(isset($gallery))
                                <input type="hidden" name="gallery_id" value={{ hashid_encode($gallery->id) }}>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 mb-2">
                        <div class="form-group">
                            <label for="">Data</label>
                            @if(isset($gallery_post))
                                <input class="form-control date" type="text" value="{{ \Carbon\Carbon::parse($gallery_post->date)->format('d/m/Y') }}" name="date" >
                            @else
                                <input class="form-control date" type="text" value="" name="date" >
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <!-- Our markup, the important part here! -->
                        <div id="drag-and-drop-zone" class="dm-uploader p-5" style="height: 500px">
                            <h3 class="mb-5 mt-5 text-center">Arraste e solte fotos e imagens</h3>
                            <div class="btn btn-primary btn-sm" style="margin-top: -50px">
                                <span>Selecione no browser</span>
                                <input type="file" title='Clique para adicionar uma imagem'/>
                            </div>
                        </div><!-- /uploader -->
                    </div>
                    <div class="col-md-6 col-sm-12" style="height: 500px">
                        <div class="card h-100">
                            <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                @if(isset($gallery->images)&&is_array(unserialize($gallery->images)))
                                   @foreach(unserialize($gallery->images) as $image)
                                        <li class="media" id="uploaderFileztulb6q1hk" path="{{ $image }}" gallery_id="{{ $gallery->id }}">
                                            <img width="50" height="50" class="mr-3 mb-2 preview-img" src="{{ $image }}" alt="Generic placeholder image">
                                            <div class="media-body mb-1">
                                                <p class="mb-2">
                                                    <strong>Imagem</strong> - Status: <span class="status text-success">Salva</span>
                                                </p>
                                                <div class="progress mb-2">
                                                    <div class="progress-bar bg-primary bg- bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
                                                    <a href="#" class="text-danger"> Remover </a>
                                                </div>
                                                <hr class="mt-1 mb-1">
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li class="text-muted text-center empty">Não há imagens ainda.</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <script type="text/html" id="files-template">
                    <li class="media">
                        <div class="media-body mb-1">
                            <input type="hidden" name="gallery-img[]">
                            <p class="mb-2">
                                <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
                            </p>
                            <div class="progress mb-2">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                                     role="progressbar"
                                     style="width: 0%"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                </div>
                                <a href="#" class="text-danger"> Remover </a>
                            </div>
                            <hr class="mt-1 mb-1" />
                        </div>
                    </li>
                </script>



                {{--    <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <!-- Our markup, the important part here! -->
                                <div id="drag-and-drop-zone" class="dm-uploader p-5">
                                    <h3 class="mb-5 mt-5 text-center">Drag &amp; drop files here</h3>

                                    <div class="btn btn-primary btn-block mb-5">
                                        <span>Selecione no browser</span>
                                        <input type="file" title='Click to add Files'/>
                                    </div>
                                </div><!-- /uploader -->
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="card h-100">
                                    <div class="card-header">
                                        Lista de imagens
                                    </div>
                                    <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                        <li class="text-muted text-center empty">Não há imagens ainda.</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- /file list -->
                        <script type="text/html" id="files-template">
                            <li class="media">
                                <div class="media-body mb-1">
                                    <input type="hidden" name="gallery-img[]">
                                    <p class="mb-2">
                                        <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
                                    </p>
                                    <div class="progress mb-2">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                                             role="progressbar"
                                             style="width: 0%"
                                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                        <a href="#" class="text-danger"> Remover </a>
                                    </div>
                                    <hr class="mt-1 mb-1" />
                                </div>
                            </li>
                        </script>
                    </div>

                    <div class="tab-pane fade" id="food-menu" role="tabpanel" aria-labelledby="nav-contact-tab">
                <textarea id="tinyEditor">


                </textarea>
                    </div>
                    <div class="tab-pane fade" id="google-info" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="">Selecionar no mapa</label>
                                <div class="google-map" style="height: 390px"></div>
                                <input type="hidden" id="latitude">
                                <input type="hidden" id="longitude">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-4">
                            <div class="form-group">
                                <label for="">ID Google Places (Caso a empresa já tenha se cadastrado no Google)</label>
                                <input class="form-control" type="text" value="ChIJN1t_tDeuEmsRUsoyG83frY4" disabled>
                            </div>
                        </div>
                    </div>--}}
            </div>
            <hr>
            <div class="flex-center">
                @if(isset($gallery_post))
                    <button class="btn btn-md btn-primary btn-update" type="submit">Atualizar</button>
                @else
                    <button class="btn btn-md btn-success" type="submit">Salvar</button>
                @endif
                    <a href="/franqueado/galerias" class="btn btn-light btn-sm">Cancelar</a>
            </div>
        </form>
        <br>
        <br>
    </div>


@endsection

