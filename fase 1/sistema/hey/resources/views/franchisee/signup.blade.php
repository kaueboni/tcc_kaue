@extends('franchisee.layouts.main-tpl')

@section('title', 'Sign Up')

@section('content')

    <div class="container-fluid pt-5 mt-lg-5">

        <h2 class="text-center pt-5 mb-4">Torne-se um franqueado</h2>

        <p class="text-center mb-4">
            Envie seus dados e da cidade a qual deseja se tornar um representante.
            Nossa equipe entrará em contato para...
        </p>

        <hr>


        <!-- Default form register -->
            <form class="mx-auto" style="width: 400px">

                <!-- Default input name -->
                <label for="defaultFormRegisterNameEx" class="grey-text">Seu nome completo</label>
                <input type="text" id="defaultFormRegisterNameEx" class="form-control">

                <br>

                <label for="defaultFormRegisterNameEx" class="grey-text">Número do seu CPF</label>
                <input type="text" id="defaultFormRegisterNameEx" class="form-control">

                <br>

                <label for="defaultFormRegisterNameEx" class="grey-text">Nome da Cidade</label>
                <input type="text" id="defaultFormRegisterNameEx" class="form-control">

                <br>

                <label for="defaultFormRegisterNameEx" class="grey-text">Estado</label>
                <input type="text" id="defaultFormRegisterNameEx" class="form-control">

                <br>

                <label for="defaultFormRegisterNameEx" class="grey-text">Número de habitantes</label>
                <input type="text" id="defaultFormRegisterNameEx" class="form-control">

                <br>


                <!-- Default input email -->
                <label for="defaultFormRegisterEmailEx" class="grey-text">Seu email</label>
                <input type="email" id="defaultFormRegisterEmailEx" class="form-control">

                <br>

                <!-- Default input email -->
                <label for="defaultFormRegisterConfirmEx" class="grey-text">Confirme seu email</label>
                <input type="email" id="defaultFormRegisterConfirmEx" class="form-control">

                <br>

                <div class="text-center mt-4">
                    <button class="btn btn-primary" type="submit">Enviar</button>
                </div>

                <br>

            </form>
            <!-- Default form register -->
    </div>







@endsection
