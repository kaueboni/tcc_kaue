@extends('franchisee.layouts.login-tpl')

@section('title', 'Sign Up')

@section('content')

    <div class="container-fluid pt-5 mt-lg-5 col-lg-3">
        <!-- Default form login -->
        <form id="formLogin">
            @csrf
            <p class="h4 text-center mb-4">Faça login na sua conta Hey Cidades</p>

            <!-- Default input email -->
            <label for="defaultFormLoginEmailEx" class="grey-text">Seu email</label>
            <input type="email" class="form-control" name="email">

            <br>

            <!-- Default input password -->
            <label for="defaultFormLoginPasswordEx" class="grey-text">Sua senha</label>
            <input type="password" class="form-control" name="password">
            <a href="" >Esqueci minha senha</a>

            <div class="text-center mt-4">
                <button class="btn btn-primary" type="submit">Login</button>
            </div>
        </form>
        <!-- Default form login -->
    </div>


@endsection
