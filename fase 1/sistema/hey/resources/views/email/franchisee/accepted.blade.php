@extends('email.layouts.main')

@section('content')

    <!-- Content : BEGIN -->
    <table role="content" cellspacing="0" cellpadding="0" border="0" align="center" class="container box-bg" width="500" style="background-color: #fff; font-family: 'Roboto', Arial, sans-serif; font-size: 15px; margin: 0 auto; max-width: 100%; width: 500px;">
        <tr>
            <td align="center" class="section" style="padding-bottom: 30px; padding-top: 0;">
                <table role="content" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" class="full-mobile main-title" style="background: #2e2f77; color: #FFF; max-width: 100%; text-align: center;">
                    <tr>
                        <td style="font-size: 18px; font-weight: bold; padding: 10px 15px; text-transform: uppercase;">
                            Sua conta foi criada!
                        </td>
                    </tr>
                </table>
                <table role="content" cellspacing="0" cellpadding="0" border="0" align="center" width="460" class="full-mobile" style="max-width: 100%;">
                    <tr>
                        <td class="text" style="color: #888; font-size: 16px; line-height: 1.6;">
                            <h3>Bem vindo,</h3>
                            <p>Sua conta foi criada com sucesso!</p>
                            <p>Faça login com o e-mail <b>{{ $user->email }}</b> e a senha <b>{{ $password }}</b></p>
                            <p>Acesse sua conta e configure suas opções. Até breve!</p>

                            <div>
                                <!--[if mso]>
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:55px;v-text-anchor:middle;width:320px;" stroke="f" fillcolor="#83ba1c">
                                    <w:anchorlock/>
                                    <center>
                                <![endif]-->
                                <a href="{{url('login')}}" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: none; background-color: #83ba1c; color: #ffffff; display: inline-block; font-family: 'Open Sans', sans-serif; font-size: 18px; font-weight: 500; line-height: 55px; margin: 0px auto 0; text-align: center; text-decoration: none; text-transform: uppercase; width: 320px;" class="button">Acessar minha conta</a>
                                <!--[if mso]>
                                </center>
                                </v:rect>
                                <![endif]-->
                            </div>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- Content : END -->

@endsection