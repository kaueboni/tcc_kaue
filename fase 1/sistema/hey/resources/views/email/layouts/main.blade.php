@include('email.layouts.header')

<main id="main-content">
    @yield('content')
</main>

@include('email.layouts.footer')