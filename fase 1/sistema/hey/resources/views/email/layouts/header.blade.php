<!DOCTYPE html>
<html lang="en" xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
    <style type="text/css">
        * {
            font-family: sans-serif !important;
        }
    </style>
    <![endif]-->

    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,800');
    </style>

    <!-- Web Font / @font-face : END -->



    <!-- CSS Reset & Embedded : BEGIN -->
    <style type="text/css">html,body{margin:0 auto !important;padding:0 !important;height:100% !important;width:100% !important;font-family:sans-serif}*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}div[style*="margin: 16px 0"]{margin:0 !important}table,td{mso-table-lspace:0pt !important;mso-table-rspace:0pt !important}table{border-spacing:0 !important;border-collapse:collapse !important;table-layout:fixed !important;margin:0 auto !important}table table table{table-layout:auto}table{max-width:100%}img{-ms-interpolation-mode:bicubic;display:block;max-width:100%;height:auto}*[x-apple-data-detectors],.x-gmail-data-detectors,.x-gmail-data-detectors *,.aBn{border-bottom:0 !important;cursor:default !important;color:inherit !important;text-decoration:none !important;font-size:inherit !important;font-family:inherit !important;font-weight:inherit !important;line-height:inherit !important}.a6S{display:none !important;opacity:0.01 !important}img.g-img+div{display:none !important}.button-link{text-decoration:none !important}@media only screen and (min-device-width: 375px) and (max-device-width: 413px){.email-container{min-width:375px !important}}@media screen and (max-width: 480px){u ~ div .email-container{min-width:100vw;width:100% !important}}table{max-width:100%}@media screen and (max-width: 700px){.full-mobile{width:100% !important;max-width:100% !important}}body,.container{font-family:"Roboto",Arial,sans-serif}h1,h2,h3{color:#2e2f77;font-weight:700}
    </style>
    <!-- CSS Reset & Embedded : END -->

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" width="100%" style="background: #f5f5f5; font-family: 'Roboto', Arial, sans-serif; font-size: 15px; margin: 0; mso-line-height-rule: exactly;">

<table role="content" cellspacing="0" cellpadding="0" border="0" align="center" class="container spaced" width="500" style="font-family: 'Roboto', Arial, sans-serif; font-size: 15px; margin: 0 auto; max-width: 100%; width: 500px;">
    <tr><td style="padding: 20px;"></td></tr>
</table>


<center id="body-container" style="background: #f5f5f5;">
    <!-- Preheader : BEGIN -->
    <table role="preheader" cellspacing="0" cellpadding="0" border="0" align="center" id="pre-header" width="500" class="email-container" style="color: #666; font-size: 12px; max-width: 100%;">
        <tr>
            <td align="left" style="padding: 20px 0;">
                Adicione <a href="mailto:nome@email.com" style="color: #666; display: inline-block;">atendimento@heycidades.com.br</a> aos seus contatos.
            </td>
            <td width="140" align="right" style="padding: 20px 0;">
                <a href="#" class="help-link" target="_blank" style="color: #666; display: inline-block; font-style: italic;">Abrir no navegador</a>
            </td>
        </tr>
    </table>
    <!-- Preheader : END -->
    <!-- HEADER : BEGIN -->
    <table role="content" cellspacing="0" cellpadding="0" border="0" align="center" id="header" class="container--gutter" width="500" style="background: #FFF; border-radius: 5px 5px 0 0; border-top: 2px solid #ddd; max-width: 100%; padding-left: 20px; padding-right: 20px;margin-bottom:30px">
        <tr>
            <td style="padding-bottom: 20px; padding-top: 20px;">
                <table role="content" cellspacing="0" cellpadding="0" border="0" align="center" width="460" class="full-mobile" style="max-width: 100%;">
                    <tr>
                       {{-- <td style="padding-bottom: 20px; padding-top: 20px;">
                            <img class="logo-main scale-with-grid" src="{{ url('admin/assets/images/logo-email.png') }}" alt="Loja IEAB" width="267" height="101" style="height: auto; width: 220px;">
                        </td>--}}
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- HEADER : END -->