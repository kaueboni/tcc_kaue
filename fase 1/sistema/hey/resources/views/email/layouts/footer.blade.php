
<!-- Footer : BEGIN -->
<table role="footer" cellspacing="0" cellpadding="0" border="0" align="center" id="footer" width="500" style="background: #2e2f77; border-radius: 0 0 5px 5px; color: #FFF; margin: auto; max-width: 100%;" class="email-container">
    <tr>
        <td align="left" style="font-size: 13px; line-height: 1.3; padding: 25px;">
            <p></p>HeyCidades Plataforma de Guias Comerciais Ltda. <br>
            CNPJ: 00.000.000/0001-50. <p></p>
            <p>R. Alvaro Coelho, 335 - Bela Vista<br>
                Presidente Epitácio/SP - CEP 19470-000</p>
        </td>
    </tr>
</table>
<!-- Footer : END -->

</center>
<table role="content" cellspacing="0" cellpadding="0" border="0" align="center" class="container subscrible" width="500" style="font-family: 'Roboto', Arial, sans-serif; font-size: 15px; margin: 0 auto; max-width: 100%; width: 500px;">
    <tr>
        <td class="container--gutter" style="color: #222; font-size: 11px; line-height: 1.2; padding: 20px 15px; padding-left: 20px; padding-right: 20px; text-align: center;">
            <p>Este e-mail foi gerado automaticamente e não deve ser respondido.</p>
            <p>Em caso de dúvidas, ligue para (18)3281-4389 .</p>
        </td>
    </tr>
</table>

<table role="content" cellspacing="0" cellpadding="0" border="0" align="center" class="container spaced" width="500" style="font-family: 'Roboto', Arial, sans-serif; font-size: 15px; margin: 0 auto; max-width: 100%; width: 500px;">
    <tr><td style="padding: 20px;"></td></tr>
</table>
</body>
</html>