@extends('company.layouts.main-tpl')

@section('title', 'Editar informações')

@section('content')

    @include('company.components.form')

@endsection

