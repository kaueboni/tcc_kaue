<?php isset( $company ) ?
    $action_title = 'Editar' : $action_title = 'Nova' ?>

<div class="container-fluid pt-5 mt-lg-5">
    <div class="row">
        <h3 class="text-left">{{ $action_title }} Empresa</h3>
    </div>
    <hr>
    <form id="formCompany">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-basic-info-tab" data-toggle="tab" href="#basic-info"
                   role="tab"
                   aria-controls="nav-home" aria-selected="true">Home</a>
                <a class="nav-item nav-link" id="nav-gallery-tab" data-toggle="tab" href="#gallery" role="tab"
                   aria-controls="nav-profile" aria-selected="false">Galeria de Fotos</a>
                <a class="nav-item nav-link" id="nav-food-menu-tab" data-toggle="tab" href="#food-menu" role="tab"
                   aria-controls="nav-contact" aria-selected="false">Cardápio</a>
                <a class="nav-item nav-link" id="nav-google-info-tab" data-toggle="tab" href="#google-info" role="tab"
                   aria-controls="nav-contact" aria-selected="false">Localização no mapa</a>
            </div>
        </nav>
        <br>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="basic-info" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="row">
                    <div class="col-lg-4 col-md-4 mb-4">
                        <div class="form-group">
                            <label for="">Nome</label>
                            <input class="form-control" type="text" value="{{ $company->name or ""}}" name="name">
                            @if(isset($company))
                                <input type="hidden" value="{{ hashid_encode($company->id) }}" name="company_id">
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Descrição Resumida
                                <small class="text-secondary">(até 200 caracteres)</small>
                            </label>
                            <textarea maxlength="200" rows="4" class="form-control" type="text"
                                      name="summary">{{ $company->summary or "" }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Descrição Detalhada
                                <small class="text-secondary">(até 500 caracteres)</small>
                            </label>
                            <textarea maxlength="500" rows="5" class="form-control" type="text"
                                      name="description">{{ $company->description or "" }}</textarea>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 mb-4">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Telefone Primário</label>
                                <input class="form-control phone_with_ddd" type="text"
                                       value="{{ $company->primary_phone or ""}}" name="primary_phone">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Telefone Secundário</label>
                                <input class="form-control phone_with_ddd" type="text"
                                       value="{{ $company->secondary_phone or ""}}" name="secondary_phone">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="">Celular/WhatsApp</label>
                                <input class="form-control cel_with_ddd" type="text"
                                       value="{{ $company->cellphone or ""}}" name="cellphone">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">E-mail</label>
                            <input class="form-control" type="email" value="{{ $company->email or ""}}" name="email">
                        </div>

                        <hr>

                        <div class="row">
                            <div class="form-group col-md-9">
                                <label for="">Endereço</label>
                                <input class="form-control" type="text" value="{{ $company->street_name or ""}}"
                                       name="street_name">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="">Número</label>
                                <input class="form-control" type="text" value="{{ $company->street_number or ""}}"
                                       name="street_number">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-5">
                                <label for="">Bairro</label>
                                <input class="form-control" type="text" value="{{ $company->district or ""}}"
                                       name="district">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="">Complem.</label>
                                <input class="form-control" type="text" value="{{ $company->complement or ""}}"
                                       name="complement">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">CEP</label>
                                <input class="form-control cep" type="text" value="{{ $company->zipcode or ""}}"
                                       name="zipcode">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 mb-4">
                        <label for="">Selecione categorias para sua empresa</label> <br>
                        <select class="multiple-categories" name="categories[]" multiple="multiple">

                            @foreach($categories as $cat)
                                @if($company_cats->contains($cat->id))
                                <option selected title="{{ $cat->id }}">{{ $cat->name }}</option>
                                @else
                                <option title="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endif
                            @endforeach

                        </select>

                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <!-- Our markup, the important part here! -->
                        <div id="drag-and-drop-zone" class="dm-uploader p-5" style="height: 250px">
                            <h3 class="mb-5 mt-5 text-center">Arraste e solte imagens</h3>
                            <div class="btn btn-primary btn-sm" style="margin-top: -50px">
                                <span>Selecione no browser</span>
                                <input type="file" title='Clique para adicionar uma imagem'/>
                            </div>
                        </div><!-- /uploader -->
                    </div>
                    <div class="col-md-6 col-sm-12" style="height: 250px">
                        <div class="card h-100">
                            <ul class="list-unstyled p-2 d-flex flex-column col" id="files">
                                @if(isset($company->gallery)&&is_array(unserialize($company->gallery)))
                                    @foreach(unserialize($company->gallery) as $image)
                                        <li class="media" id="uploaderFileztulb6q1hk" path="{{ $image }}">
                                            <img width="50" height="50" class="mr-3 mb-2 preview-img" src="{{ $image }}"
                                                 alt="Generic placeholder image">
                                            <div class="media-body mb-1">
                                                <p class="mb-2">
                                                    <strong>Imagem</strong> - Status: <span class="status text-success">Salva</span>
                                                </p>
                                                <div class="progress mb-2">
                                                    <div class="progress-bar bg-primary bg- bg-success"
                                                         role="progressbar" style="width: 100%;" aria-valuenow="100"
                                                         aria-valuemin="0" aria-valuemax="100">100%
                                                    </div>
                                                    <a href="#" class="text-danger"> Remover </a>
                                                </div>
                                                <hr class="mt-1 mb-1">
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li class="text-muted text-center empty">Não há imagens ainda.</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <script type="text/html" id="files-template">
                    <li class="media">
                        <div class="media-body mb-1">
                            <p class="mb-2">
                                <strong>%%filename%%</strong> - Status: <span class="text-muted">Waiting</span>
                            </p>
                            <div class="progress mb-2">
                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                                     role="progressbar"
                                     style="width: 0%"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                </div>
                                <a href="#" class="text-danger"> Remover </a>
                            </div>
                            <hr class="mt-1 mb-1"/>
                        </div>
                    </li>
                </script>
            </div>

            <div class="tab-pane fade" id="food-menu" role="tabpanel" aria-labelledby="nav-contact-tab">
            <textarea id="tinyEditor" name="menu" style="height: 500px">
                  {{ $company->menu or "" }}
            </textarea>
            </div>
            <div class="tab-pane fade" id="google-info" role="tabpanel" aria-labelledby="nav-contact-tab">
                <div class="col-lg-12 col-md-12">
                    <div class="form-group">
                        <label for="">Selecionar no mapa</label>
                        <div class="google-map" style="height: 390px"></div>
                        <input type="hidden" id="latitude" name="latitude"
                               value="{{ $company->latitude or '-21.766494' }}">
                        <input type="hidden" id="longitude" name="longitude"
                               value="{{ $company->longitude or '-52.1155447' }}">
                    </div>
                </div>
                {{--<div class="col-lg-4 col-md-4 mb-4">
                    <div class="form-group">
                        <label for="">ID Google Places (Caso a empresa já tenha se cadastrado no Google)</label>
                        <input class="form-control" type="text" value="ChIJN1t_tDeuEmsRUsoyG83frY4" disabled>
                    </div>
                </div>--}}
            </div>
        </div>
        <hr>
        <div class="flex-center">
            @if(empty($company))
                <button class="btn btn-md btn-success" type="submit">Salvar</button>
            @else
                <button class="btn btn-md btn-primary btn-update" type="submit">Atualizar</button>
            @endif
            <button class="btn btn-md btn-light" type="submit">Cancelar</button>
        </div>
        <br>
        <br>
    </form>
</div>
