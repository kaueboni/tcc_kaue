<div class="container-fluid pt-5 mt-lg-5">
    <div class="card mb-4">
        <div class="card-header">
            <h3 class="card-title" style="margin-left: 20px; margin-top: 12px">Empresas</h3>
        </div>
        <div class="card-body">
            <table id="companies_table" class="datatable table table-striped" style="width:100%">
                @if(isset($companies)&&$companies->count() > 0)
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Cidade</th>
                        <th>Nome do Responsável</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->franchisee->city_name }}</td>
                            <td>{{ $company->user->name }}</td>
                            <td>
                                <a href="{{route('company',['city'=>$company->franchisee->slug,'company'=>$company->id])}}"><i class="material-icons md-24 text-info">remove_red_eye</i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
            @else
                <p>Não há empresas cadastradas.</p>
            @endif
        </div>
    </div>
</div>
