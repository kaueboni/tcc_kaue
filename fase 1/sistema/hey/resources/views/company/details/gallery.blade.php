@extends('company.layouts.main-tpl')

@section('title', 'Cardápio')

@section('content')
    <div class="container-fluid pt-5 mt-lg-5">

        <br>
        <h1>Olá, Nome do Responsável</h1>
        <h5>Confira um balanço geral da sua empresa no HeyCidades.</h5>
        <br>

        <div class="row wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

            <!--Grid column-->
            <div class="col-lg-3 col-md-12 mb-4">
                <!--Card-->
                <a href="">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header">Novos cliques</div>
                        <!--Card content-->
                        <div class="card-body">
                            <h7>Este mês</h7><br>
                            <h2>54</h2><br>
                            <h7>Mês passado</h7><br>
                            <h2>39</h2>
                        </div>
                    </div>
                </a>
                <!--/.Card-->
            </div>
            <!--Grid column-->


        </div>

    </div>






@endsection
