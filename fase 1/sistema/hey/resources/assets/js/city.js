const basename = document.querySelector("meta[name='url']").getAttribute("content");

function initMap() {

    var map = $('.google-map');
    if($('[name=location_name]').val() != null){
        var location_name = $('[name=location_name]').val();
    }else{
        var location_name = $('[name=location_name]').val();
    }

    map.googleMap({
        zoom: 15,
    });
    map.addMarker({
        title: location_name,
        text: 'Posicione no local apropriado',
        coords: [$('#latitude').val(), $('#longitude').val()], //valor dos inputs de latitude e longitude
        draggable: true,
        success: function (e) {
            $("#latitude").val(e.lat);
            $("#longitude").val(e.lon);
        }
    });
}

function saveFranchiseeCandidate(){

    $('#franchiseeModal').find('.btn-success').on('click',function(){

        const api = basename + '/api/franchisees/candidates';

        const formData = $('#franchiseeSignUpForm').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('Seus dados foram enviados.','success');
                $('#franchiseeModal').modal('hide');
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function saveCompanyCandidate(){

    $('#companySignUpForm').find('.btn-success').on('click',function(){

        const api = basename + '/api/companies/candidates';

        const formData = $('#companySignUpForm').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('Seus dados foram enviados. Em breve você receberá um e-mail com os dados de login para que possa manter as informações de sua empresa atualizadas.','success');
                $('#franchiseeModal').modal('hide');
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


function initMasks(){
    $('.date').mask('00/00/0000');
    $('.time').mask('00:00:00');
    $('.date_time').mask('00/00/0000 00:00:00');
    $('.cep').mask('00000-000');
    $('.phone').mask('0000-0000');
    $('.phone_with_ddd').mask('(00)0000-0000');
    $('.cel_with_ddd').mask('(00)00000-0000');
    $('.phone_us').mask('(000) 000-0000');
    $('.mixed').mask('AAA 000-S0S');
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.money').mask('000.000.000.000.000,00', {reverse: true});
    $('.money2').mask("#.##0,00", {reverse: true});
    $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
            'Z': {
                pattern: /[0-9]/, optional: true
            }
        }
    });
    $('.ip_address').mask('099.099.099.099');
    $('.percent').mask('##0,00%', {reverse: true});
    $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
    $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
    $('.fallback').mask("00r00r0000", {
        translation: {
            'r': {
                pattern: /[\/]/,
                fallback: '/'
            },
            placeholder: "__/__/____"
        }
    });
    $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
}



$(document).ready(function(){
    initMasks();
    initMap()
    saveFranchiseeCandidate();
    saveCompanyCandidate();
});