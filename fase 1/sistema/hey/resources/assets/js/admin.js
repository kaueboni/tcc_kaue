const basename = document.querySelector("meta[name='url']").getAttribute("content");

function example() {
    console.log('function example called');
}

window.example = example;

//inicializando mapa em branco
function initMap() {

    var map = $('.google-map');
    if($('[name=location_name]').val() != null){
        var location_name = $('[name=location_name]').val();
    }else{
        var location_name = $('[name=location_name]').val();
    }

    map.googleMap({
        zoom: 15,
    });
    map.addMarker({
        title: location_name,
        text: 'Posicione no local apropriado',
        coords: [$('#latitude').val(), $('#longitude').val()], //valor dos inputs de latitude e longitude
        draggable: true,
        success: function (e) {
            $("#latitude").val(e.lat);
            $("#longitude").val(e.lon);
        }
    });
}

tinymce.init({
    selector: '#tinyEditor',
    //height: 300,
    menubar: false,
    plugins: [
        'advlist autolink lists link image charmap print preview anchor textcolor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code help wordcount'
    ],
    toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css']
});

function initRepeater() {

    $('.repeater').repeater({
        // (Optional)
        // start with an empty list of repeaters. Set your first (and only)
        // "data-repeater-item" with style="display:none;" and pass the
        // following configuration flag
        initEmpty: true,
        // (Optional)
        // "defaultValues" sets the values of added items.  The keys of
        // defaultValues refer to the value of the input's name attribute.
        // If a default value is not specified for an input, then it will
        // have its value cleared.
        defaultValues: {
            'text-input': 'foo'
        },
        /*// (Optional)
        // "show" is called just after an item is added.  The item is hidden
        // at this point.  If a show callback is not given the item will
        // have $(this).show() called on it.
        show: function () {
            $(this).slideDown();
        },
        // (Optional)
        // "hide" is called when a user clicks on a data-repeater-delete
        // element.  The item is still visible.  "hide" is passed a function
        // as its first argument which will properly remove the item.
        // "hide" allows for a confirmation step, to send a delete request
        // to the server, etc.  If a hide callback is not given the item
        // will be deleted.
        hide: function (deleteElement) {
            if(confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },
        // (Optional)
        // You can use this if you need to manually re-index the list
        // for example if you are using a drag and drop library to reorder
        // list items.
        ready: function (setIndexes) {
            $dragAndDrop.on('drop', setIndexes);
        },
        // (Optional)
        // Removes the delete button from the first list item,
        // defaults to false.*/
        isFirstItemUndeletable: true
    });
}

/*FRANCHISEES CANDIDATES FUNCTIONS*/

function removeFranchiseeCandidate() {

    $('.candidate-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('candidate-id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover um candidato a franqueado do HeyCidades!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/franchisees/candidates/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('O candidato foi removido.','success');
                            elem.remove();
                        },
                        error: function(data){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })

}

function acceptFranchiseeCandidate() {

    $('.franchisee-candidate-accept').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('candidate-id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Já analisou os dados do candidato? Você está prestes a aprovar uma franquia do HeyCidades!",
            icon: "info",
            buttons: true,
            dangerMode: false,
        })
            .then((willAccept) => {
                if (willAccept) {

                    const api = basename + '/api/franchisees/candidates/accept/' + id;

                    $.ajax({
                        url: api,
                        method: 'GET',
                        type: 'GET',
                        //dataType: 'json',
                        success: function(){
                            $.notify('Parabéns! Mais uma franquia do HeyCidades foi criada.','success');
                            elem.remove();
                        },
                        error: function(data){
                            $.notify(data.responseJSON.message,'error');
                        }
                    });

                }
            });

    })

}


/*COMPANIES CANDIDATES FUNCTIONS*/

function removeCompanyCandidate() {

    $('.company-candidate-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('candidate-id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma empresa em aprovação do HeyCidades!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/companies/candidates/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('A empresa foi removida.','success');
                            elem.remove();
                        },
                        error: function(data){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });
                }
            });
    })

}

function acceptCompanyCandidate() {

    $('.candidate-accept').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('candidate-id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Já analisou os dados do empresa? Você está prestes a aprovar uma empresa!",
            icon: "info",
            buttons: true,
            dangerMode: false,
        })
            .then((willAccept) => {
                if (willAccept) {

                    const api = basename + '/api/companies/candidates/accept/' + id;

                    $.ajax({
                        url: api,
                        method: 'GET',
                        type: 'GET',
                        //dataType: 'json',
                        success: function(){
                            $.notify('Parabéns! Mais uma empresa do HeyCidades foi criada.','success');
                            elem.remove();
                        },
                        error: function(data){
                            $.notify(data.responseJSON.message,'error');
                        }
                    });

                }
            });

    })

}


/*EVENTS METHODS*/
function saveEvent(){
    $('#formEvent').find('.btn-success').on('click',function(e){

        e.preventDefault();

        const api = basename + '/api/events';

        const formData = $('#formEvent').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        formData['info'] = tinyMCE.get('tinyEditor').getContent();

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);


        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('O evento foi salvo.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/eventos';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeEvent(){
    $('.event-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover um evento!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/events/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('O evento foi removido.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })
}

function updateEvent(){
    $('#formEvent').find('.btn-update').on('click',function(e){

        e.preventDefault();

        const formData = $('#formEvent').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        formData['info'] = tinyMCE.get('tinyEditor').getContent();

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        const api = basename + '/api/events/' + formData['event_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('O evento foi atualizado.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/eventos';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* GALLERY POSTS METHODS */
function saveGalleryPost(){
    $('#formGallery').find('.btn-success').on('click',function(e){

        e.preventDefault();

        const api = basename + '/api/gallery_posts';

        const formData = $('#formGallery').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('O galeria foi salva.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/galerias';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeGalleryPost(){
    $('.gallery-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma galeria de fotos!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/gallery_posts/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('O galeria de fotos foi removida.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })
}

function updateGalleryPost(){
    $('#formGallery').find('.btn-update').on('click',function(e){

        e.preventDefault();

        const formData = $('#formGallery').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        const api = basename + '/api/gallery_posts/' + formData['gallery_post_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('A galeria de fotos foi atualizada.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/galerias';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* TOURSPOTS METHODS */
function saveTourSpot(){
    $('#formTourSpot').find('.btn-success').on('click',function(e){

        e.preventDefault();

        const api = basename + '/api/tourspots';

        const formData = $('#formTourSpot').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        formData['info'] = tinyMCE.get('tinyEditor').getContent();

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('O ponto turístico foi salvo.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/pontos-turisticos';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeTourSpot(){
    $('.tourspot-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover um ponto turístico!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/tourspots/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('O ponto turístico foi removido.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })
}

function updateTourSpot(){
    $('#formTourSpot').find('.btn-update').on('click',function(e){
        e.preventDefault();



        const formData = $('#formTourSpot').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        formData['info'] = tinyMCE.get('tinyEditor').getContent();

        const api = basename + '/api/tourspots/'+formData['tourspot_id'];


        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('O ponto turístico foi atualizado.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/pontos-turisticos';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* NEWS POSTS METHODS */
function saveNewsPost(){
    $('#formNews').find('.btn-success').on('click',function(e){

        e.preventDefault();

        const api = basename + '/api/news';

        const formData = $('#formNews').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        formData['content'] = tinyMCE.get('tinyEditor').getContent();

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('A notícia foi salva.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/noticias';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeNewsPost(){
    $('.news-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma notícia!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/news/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('A notícia foi removida.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })
}

function updateNewsPost(){
    $('#formNews').find('.btn-update').on('click',function(e){

        e.preventDefault();

        const formData = $('#formNews').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        formData['content'] = tinyMCE.get('tinyEditor').getContent();

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData
        }

        console.log(request);

        const api = basename + '/api/news/' + formData['news_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('A notícia foi atualizada.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/noticias';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* CATEGORY DEFAULT METHODS */
function saveCategoryDefault(){
    $('#formCategoryDefault').find('.btn-success').on('click',function(e){

        e.preventDefault();

        const api = basename + '/api/categories_defaults';

        const formData = $('#formCategoryDefault').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('A categoria foi salva.','success');
                setTimeout(function(){
                    window.location.href = basename + '/admin/categorias';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeCategoryDefault(){
    $('.cat-default-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma categoria!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/categories_defaults/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('A categoria foi removida.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })
}

function updateCategoryDefault(){
    $('#formCategoryDefault').find('.btn-update').on('click',function(e){

        e.preventDefault();

        const formData = $('#formCategoryDefault').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        const api = basename + '/api/categories_defaults/' + formData['category_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('A categoria foi atualizada.','success');
                setTimeout(function(){
                    window.location.href = basename + '/admin/categorias';
                }, 500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* CATEGORY METHODS */
function saveCategory(){
    $('#formCategory').find('.btn-success').on('click',function(e){

        e.preventDefault();

        const api = basename + '/api/categories';

        const formData = $('#formCategory').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('A categoria foi salva.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/categorias';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeCategory(){
    $('.category-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma categoria!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/categories/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('A categoria foi removida.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });
                }
            });

    })
}

function updateCategory(){
    $('#formCategory').find('.btn-update').on('click',function(e){

        e.preventDefault();

        const formData = $('#formCategory').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        const api = basename + '/api/categories/' + formData['category_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('A categoria foi atualizada.','success');
                setTimeout(function(){
                    window.location.href = basename + '/franqueado/categorias';
                }, 500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* PLAN METHODS */
function savePlan(){
    $('#formPlan').find('.btn-success').on('click',function(e){
        e.preventDefault();

        const api = basename + '/api/plans';

        const formData = $('#formPlan').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        console.log(formData);

        $.ajax({
            url: api,
            method: 'POST',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('O plano foi salvo.','success');
                setTimeout(function(){
                    window.location.href = basename + '/admin/planos';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removePlan(){
    $('.plan-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover um plano de anúncio!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/plans/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('O plano foi removido.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });
                }
            });

    });
}

function updatePlan(){
    $('#formPlan').find('.btn-primary').on('click',function(e){
        console.log('oi');

        e.preventDefault();

        const formData = $('#formPlan').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        const api = basename + '/api/plans/' + formData['plan_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: formData,
            dataType: 'json',
            success: function(){
                $.notify('O plano foi atualizado.','success');
                setTimeout(function(){
                    window.location.href = basename + '/admin/planos';
                }, 500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}


/* COMPANY METHODS */
function updateCompany(){
    $('#formCompany').find('.btn-update').on('click',function(e){

        e.preventDefault();

        const formData = $('#formCompany').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});
        const gallery = [];
        const categories = [];
        let categories_selector = $('.select2-selection__choice');

        $.each(categories_selector,function(key,cat){
            categories.push($(cat).attr('title'));
        });

        formData['menu'] = tinyMCE.get('tinyEditor').getContent();

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        const request = {
            gallery: gallery,
            formData: formData,
            categories: categories
        }

        const api = basename + '/api/companies/' + formData['company_id'];

        $.ajax({
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            data: request,
            dataType: 'json',
            success: function(){
                $.notify('A empresa foi atualizada.','success');
                setTimeout(function(){
                    window.location.href = basename + '/empresa/editar';
                }, 1500);
            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });
    });
}

function removeCompany(){

    $('.company-remove').on('click', function (e) {
        e.preventDefault();

        const id = $(this).attr('company-id');
        const elem = $(this).parents().eq(1);

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma empresa do HeyCidades!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/companies/' + id;

                    $.ajax({
                        url: api,
                        method: 'DELETE',
                        type: 'DELETE',
                        dataType: 'json',
                        success: function(){
                            $.notify('A empresa foi removida.','success');
                            elem.remove();
                        },
                        error: function(data){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });
                }
            });
    })

}


/* REMOVE IMAGE GLOBAL FUNCTION*/
function removeImage(){
    $('.media').find('.text-danger').on('click',function(e){
        e.preventDefault();

        var elem = $(this).parents().eq(2);
        var path = elem.attr('path');
        var gallery_id = elem.attr('gallery_id');

        console.log('path',path);

        const gallery = [];

        $('.media').each(function(){
            gallery.push($(this).attr('path'))
        });

        swal({
            title: "Você tem certeza?",
            text: "Você está prestes a remover uma imagem!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    const api = basename + '/api/upload/image/delete';

                    $.ajax({
                        url: api,
                        method: 'POST',
                        type: 'POST',
                        data: {
                            path:path,
                            gallery_id:gallery_id,
                            gallery:gallery
                        },
                        dataType: 'json',
                        success: function(){
                            $.notify('A imagem foi removida.','success');
                            elem.remove();
                        },
                        error: function(){
                            $.notify('Não foi possível remover.','danger');
                        }
                    });

                }
            });

    })

}


/* AUTH FUNCTIONS*/
function login(){

    $('#formLogin').find('.btn-primary').on('click',function(e){

        e.preventDefault();
        const api = basename + '/api/login';
        const formData = $('#formLogin').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: api,
            method: 'POST',
            type: 'POST',
            data: formData,
            dataType: 'json',
            success: function(data){
                let actor;
                switch(data.code){
                    case 1 :
                        actor = 'Empresa';
                        route = '/empresa';
                        break;
                    case 2 :
                        actor = 'Franqueado';
                        route = '/franqueado';
                        break;
                    case 3 :
                        actor = 'Administrador';
                        route = '/admin';
                        break;
                }
                $.notify('Logado como '+ actor +'!','success');
                setTimeout(function(){
                    window.location.href = basename + route;
                    }, 2000);
            },
            error: function(data){
                $.notify(data.message,'warning');
            }
        });
    });
}


/*USER FUNCTIONS*/

function userUpdate(){

    $('#formUserInfo').find('.btn-primary').on('click', function(e){

        e.preventDefault();

        const formData = $('#formUserInfo').serializeArray().reduce(function (a, x) {
            a[x.name] = x.value;
            return a;
        }, {});

        console.log(formData);

        const api = basename + '/api/users/update';

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: api,
            method: 'PATCH',
            type: 'PATCH',
            dataType: 'json',
            data: formData,
            success: function(data){
                let actor;
                switch(data.code){
                    case 1 :
                        actor = 'Empresa';
                        route = '/empresa';
                        break;
                    case 2 :
                        actor = 'Franqueado';
                        route = '/franqueado';
                        break;
                    case 3 :
                        actor = 'Administrador';
                        route = '/admin';
                        break;
                }
                $.notify('Pronto! Informações atualizadas, agora já pode começar a utilizar as funcionalidades da sua franquia HeyCidades F.','success');
                setTimeout(function(){
                    window.location.href = basename + route;
                }, 2000);

            },
            error: function(data){
                $.notify(data.responseJSON.message,'error');
            }
        });

    });

}

function initMasks(){
    $('.date').mask('00/00/0000');
    $('.time').mask('00:00');
    $('.date_time').mask('00/00/0000 00:00:00');
    $('.cep').mask('00000-000');
    $('.phone').mask('0000-0000');
    $('.phone_with_ddd').mask('(00)0000-0000');
    $('.cel_with_ddd').mask('(00)00000-0000');
    $('.phone_us').mask('(000) 000-0000');
    $('.mixed').mask('AAA 000-S0S');
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.money').mask('000.000.000.000.000,00', {reverse: true});
    $('.money2').mask("#.##0,00", {reverse: true});
    $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
        translation: {
            'Z': {
                pattern: /[0-9]/, optional: true
            }
        }
    });
    $('.ip_address').mask('099.099.099.099');
    $('.percent').mask('##0,00%', {reverse: true});
    $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
    $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
    $('.fallback').mask("00r00r0000", {
        translation: {
            'r': {
                pattern: /[\/]/,
                fallback: '/'
            },
            placeholder: "__/__/____"
        }
    });
    $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
}

$(document).ready(function () {
    $('.datatable').DataTable({
        responsive: true,
    });

    $('.multiple-categories').select2();

    updateCompany();
    removeCompany();

    saveCategory();
    removeCategory();
    updateCategory();

    saveCategoryDefault();
    removeCategoryDefault();
    updateCategoryDefault();

    saveEvent();
    removeEvent();
    updateEvent();

    saveGalleryPost();
    updateGalleryPost();
    removeGalleryPost();

    saveNewsPost();
    updateNewsPost();
    removeNewsPost();

    saveTourSpot();
    updateTourSpot();
    removeTourSpot();


    savePlan();
    removePlan();
    updatePlan();

    userUpdate();

    removeImage();

    initMasks();
    initMap();
    initRepeater();

    removeFranchiseeCandidate();
    acceptFranchiseeCandidate();

    removeCompanyCandidate();
    acceptCompanyCandidate();

    login();
});


