
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



window.$ = window.jQuery = require('jquery')
require('bootstrap')
require('dm-file-uploader/dist/js/jquery.dm-uploader.min')
require('./../plugins/jquery.googlemap')
require('jquery.repeater/jquery.repeater')
require('datatables.net/js/jquery.dataTables')
require('datatables.net-bs4/js/dataTables.bootstrap4')
require('./plugins/notify.min')
require('sweetalert')
require('jquery-mask-plugin')
require('select2/dist/js/select2')
require('./drop_area')
require('popper.js')
require('./admin')







