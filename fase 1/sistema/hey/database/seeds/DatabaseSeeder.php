<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        $admin = [
            'name'     => 'Kauê',
            'email'    => 'admin@admin.com.br',
            'password' => Hash::make( 'Deusfiel1331#' ),
            'level'    => 3
        ];

        $categories = [

            [
                'name' => 'Onde ir',
                'slug' => 'onde-ir',
                'level' => 1
            ],

            [
                'name' => 'Onde comer',
                'slug' => 'onde-comer',
                'level' => 1
            ],

            [
                'name' => 'Onde ficar',
                'slug' => 'onde-ficar',
                'level' => 1
            ],

            [
                'name' => 'Serviços',
                'slug' => 'servicos',
                'level' => 1
            ],

        ];

        foreach($categories as $cat){
            \App\Models\CategoryDefault::create($cat);
        }

        $user           = new \App\User();
        $user->name     = $admin['name'];
        $user->email    = $admin['email'];
        $user->password = $admin['password'];
        $user->level    = $admin['level'];
        $user->save();


    }
}
