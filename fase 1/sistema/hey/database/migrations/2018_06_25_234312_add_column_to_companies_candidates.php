<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToCompaniesCandidates extends Migration
{

    public function up()
    {
        Schema::table('companies_candidates', function (Blueprint $table){
            $table->integer('franchisee_id')->unsigned()->index('companies_franchisee_id_foreign');
            $table->foreign('franchisee_id')->references('id')->on('franchisees')->onDelete('CASCADE');
        });
    }


    public function down()
    {
        Schema::table('companies_candidates', function (Blueprint $table){
            $table->dropColumn('franchisee_id');
        });
    }
}




