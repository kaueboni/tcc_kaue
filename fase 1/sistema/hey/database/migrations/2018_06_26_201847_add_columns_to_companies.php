<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table){
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->text('menu')->nullable();
            $table->text('gallery')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table){
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
            $table->dropColumn('menu');
            $table->dropColumn('gallery');
        });
    }
}
