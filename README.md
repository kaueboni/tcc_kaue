﻿## TCC

## HeyCidades: Plataforma para Guias de Cidades

Trabalho de conclusão de curso apresentado à banca examinadora do Instituto Federal de Educação, Ciência e Tecnologia de São Paulo - Campus Presidente Epitácio, como requisito parcial à obtenção do grau de Tecnólogo em Análise e Desenvolvimento de Sistemas. Na primeira fase, sob a orientação do professor Anderson Roberto Deizepe e na segunda fase professor Claudio Zaina.

---

## Escopo

O sistema HeyCidades consiste em uma plataforma on-line que visa disponibilizar informações sobre pequenas e médias cidades (até 300 mil habitantes), seus estabelecimentos e empresas.

Na plataforma as pessoas poderão candidatar-se a franqueados e representantes. O franqueado é quem mantém a página de uma cidade no HeyCidades atualizada com notícias, galerias de fotos, eventos e pontos turísticos do município. Os representantes são vendedores autorizados do HeyCidades, que podem atuar em uma ou mais cidades, cadastrando empresas e as oferecendo planos de anúncio, sendo comissionados pelos planos vendidos. As empresas também poderão ser cadastradas gratuitamente sem a necessidade de um representante. 

Os planos de anúncio são serviços oferecidos pela plataforma para as empresas que possibilitam que sejam adicionadas informações extras na página das empresas dentro da plataforma. Cada plano possuirá um grupo de características, um valor e um intervalo de cobrança que poderá ser mensal ou anual. As características dos planos são informações, tais como localização no mapa, números de telefone, galeria de fotos, cardápio e ofertas de produtos e serviços que as empresas podem adicionar em suas páginas. 

O pagamento de um plano de anúncio para uma empresa é dividido entre a plataforma, o franqueado da cidade na qual a empresa está localizada e, se houver, o representante. A plataforma, os franqueados e os representantes possuirão contas na plataforma e os valores dos pagamentos são creditados a elas. A plataforma permitirá que sejam realizadas transferências dos valores disponíveis para suas contas bancárias.
